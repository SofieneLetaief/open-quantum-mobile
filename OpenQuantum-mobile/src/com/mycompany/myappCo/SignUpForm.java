/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package com.mycompany.myappCo;

import Entites.Utilisateur;
import Services.AuthentificationSerivce;
import Utils.CurrentSession;
import static Utils.Statics.accountSID;
import static Utils.Statics.authToken;
import static Utils.Statics.fromPhone;
import com.codename1.components.FloatingHint;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.util.Resources;
import com.codename1.util.Base64;
import java.util.Map;

/**
 * Signup UI
 *
 * @author Shai Almog
 */
public class SignUpForm extends BaseForm {

    public SignUpForm(Resources res) {
        super(new BorderLayout());
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        tb.setUIID("Container");
        getTitleArea().setUIID("Container");
        Form previous = Display.getInstance().getCurrent();
        tb.setBackCommand("", e -> previous.showBack());
        setUIID("SignIn");

        TextField username = new TextField("", "Username", 20, TextField.ANY);
        TextField email = new TextField("", "E-Mail", 20, TextField.EMAILADDR);
        //NOM PRENOM TEL
        TextField nomTF = new TextField("", "Nom", 20, TextField.ANY);
        TextField prenomTF = new TextField("", "Prenom", 20, TextField.ANY);
        TextField numtelTf = new TextField("", "Num Tel", 20, TextField.ANY);
        //
        TextField password = new TextField("", "Password", 20, TextField.PASSWORD);
        TextField confirmPassword = new TextField("", "Confirm Password", 20, TextField.PASSWORD);
        username.setSingleLineTextArea(false);
        email.setSingleLineTextArea(false);
        //nom prenom
        nomTF.setSingleLineTextArea(false);
        prenomTF.setSingleLineTextArea(false);
        //
        password.setSingleLineTextArea(false);
        confirmPassword.setSingleLineTextArea(false);
        Button next = new Button("Next");
        Button signIn = new Button("Sign In");
        signIn.addActionListener(e -> new SignInForm(res).show());
        signIn.setUIID("Link");
        Label alreadHaveAnAccount = new Label("Already have an account?");

        Container content = BoxLayout.encloseY(
                new Label("Sign Up", "LogoLabel"),
                new FloatingHint(username),
                createLineSeparator(),
                new FloatingHint(email),
                createLineSeparator(),
                new FloatingHint(nomTF),
                createLineSeparator(),
                new FloatingHint(prenomTF),
                createLineSeparator(),
                new FloatingHint(numtelTf),
                createLineSeparator(),
                new FloatingHint(password),
                createLineSeparator(),
                new FloatingHint(confirmPassword),
                createLineSeparator()
        );
        content.setScrollableY(true);
        add(BorderLayout.CENTER, content);
        add(BorderLayout.SOUTH, BoxLayout.encloseY(
                next,
                FlowLayout.encloseCenter(alreadHaveAnAccount, signIn)
        ));
        next.requestFocus();
        next.addActionListener((ActionEvent e) -> {

            //Creating user
            String nom = nomTF.getText().toString();
            String prenom = prenomTF.getText().toString();
            int numTel = Integer.parseInt(numtelTf.getText().toString());
            String usernameTemp = username.getText().toString();
            String passwordTemp = password.getText().toString();
            String emailTemp = email.getText().toString();

            Utilisateur user = new Utilisateur(CurrentSession.id_societe, nom, prenom, emailTemp, numTel, usernameTemp, passwordTemp);
            System.out.println(user);

            //Adding user here
            boolean result = AuthentificationSerivce.getInstance().addUserToSciete(user);
//////////////////MATNA7IIIICH EL CMTTT
            //////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT

//            if (result) {
//                Response<Map> smsresult = Rest.post("https://api.twilio.com/2010-04-01/Accounts/" + accountSID + "/Messages.json").
//                        queryParam("To", "+21696250615").
//                        queryParam("From", fromPhone).
//                        queryParam("Body", "Welcome To Open Quantum Mr."+nom+" "+prenom+" "
//                                + ". Your account has been created!"
//                                + " You can now login with the username :"+usernameTemp).
//                        header("Authorization", "Basic " + Base64.encodeNoNewline((accountSID + ":" + authToken).getBytes())).
//                        getAsJsonMap();;
//
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
            System.out.println("Login Status : " + result);
            //Switching form
            new SignInForm(res).show();
        });
    }

}
