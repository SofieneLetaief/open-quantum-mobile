/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Article;
import Entites.Review;
import Services.ReviewService;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SignatureComponent;
import com.codename1.components.SpanLabel;
import com.codename1.gif.GifImage;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.Log;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.processing.Result;
import com.codename1.ui.AutoCompleteTextField;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.*;
import com.codename1.ui.Component;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.Form;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.list.DefaultListModel;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

/**
 *
 * @author Mannai
 */
public class ReviewForm extends BaseForm {

    private Resources testTheme;
    private Form current;

    public ReviewForm(Resources res,Article art) {
        super("New Review", BoxLayout.y());
         Toolbar tb = new Toolbar(true);
         setToolbar(tb);
          Form previous = Display.getInstance().getCurrent();
        getContentPane().setScrollVisible(false);

        tb.setBackCommand("", e -> previous.showBack());

        testTheme = res;
        String testKeyTest = "AIzaSyDZt_9RcYvqfLVpUptCOK3ju_iFlnSv8IE";

        Container hi = new Container(new BoxLayout(BoxLayout.Y_AXIS));
        Label desc = new Label("Description :");
        TextArea rev = new TextArea(8, 20);
        Label adr = new Label("Your Address :");
        Label sign = new Label("Your Signature :");
        Button btn = new Button("Send Review");
        SignatureComponent sig = new SignatureComponent();
        sig.addActionListener((evt) -> {
            System.out.println("The signature was changed");
            Image img = sig.getSignatureImage();

        });
           
        final DefaultListModel<String> options = new DefaultListModel<>();
        AutoCompleteTextField ac = new AutoCompleteTextField(options);
        ac = new AutoCompleteTextField(options) {
            @Override
            protected boolean filter(String text) {
                if (text.length() == 0) {
                    return false;
                }
                String[] l = searchLocations(text);
                if (l == null || l.length == 0) {
                    return false;
                }

                options.removeAll();
                for (String s : l) {
                    options.addItem(s);
                }
                return true;
            }
        };
        final String labelText;
        ac.addActionListener(e->{

        });
        btn.addActionListener(e->{
               System.out.println("test");
           
               Review r = new Review(1, rev.getText(), options.getItemAt(options.getSelectedIndex()), null, 1, art, null);
               ReviewService rs = new ReviewService();
               rs.addReviewToArticle(r);
    });
        
        ac.setMinimumElementsShownInPopup(5);
        ac.getAllStyles().setMargin(Component.LEFT, 5);
        ac.getAllStyles().setMargin(Component.RIGHT, 5);
        ac.getAllStyles().setBorder(Border.createLineBorder(10, 0x000000));
        hi.add(desc);
        hi.add(rev);
        hi.add(adr);
        hi.add(ac);
        hi.add(sign);
        hi.addComponent(sig);
        hi.add(btn);
        add(hi);

    }

    public String[] searchLocations(String text) {
        String testKeyTest = "AIzaSyDZt_9RcYvqfLVpUptCOK3ju_iFlnSv8IE";
        try {
            if (text.length() > 0) {
                ConnectionRequest r = new ConnectionRequest();
                r.setPost(false);
                r.setUrl("https://maps.googleapis.com/maps/api/place/autocomplete/json");
                r.addArgument("key", testKeyTest);
                r.addArgument("input", text);
                NetworkManager.getInstance().addToQueueAndWait(r);
                Map<String, Object> result = new JSONParser().parseJSON(new InputStreamReader(new ByteArrayInputStream(r.getResponseData()), "UTF-8"));
                String[] res = Result.fromContent(result).getAsStringArray("//description");
                return res;
            }
        } catch (Exception err) {
            Log.e(err);
        }
        return null;
    }
}
