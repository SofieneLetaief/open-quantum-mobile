/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import com.codename1.codescan.CodeScanner;
import com.codename1.codescan.ScanResult;
import com.codename1.ui.Container;
import com.codename1.ui.Form;
import com.codename1.ui.Label;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;

/**
 *
 * @author HP OMEN
 */
public class TestingForm extends Form {
    
     private Resources testTheme;

    public TestingForm(Resources res) {
        
          super("Livraison", BoxLayout.y());
           testTheme=res;
           Label test = new Label("Test");
           add(test);
           
           Container cnt = new Container(BoxLayout.y());
           
            CodeScanner.getInstance().scanQRCode(new ScanResult() {

        public void scanCompleted(String contents, String formatName, byte[] rawBytes) {
            //barCode.setText("Bar: " + contents);
            cnt.addComponent(new Label(contents));
            add(cnt);
            cnt.revalidate();
        }

        public void scanCanceled() {
            System.out.println("cancelled");
        }

        public void scanError(int errorCode, String message) {
            System.out.println("err " + message);
        }
    });
    }
    
}
