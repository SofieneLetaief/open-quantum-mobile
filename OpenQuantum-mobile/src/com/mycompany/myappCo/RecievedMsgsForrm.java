/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Article;
import Entites.FluxMsg;
import Entites.Utilisateur;
import Services.ArticleService;
import Services.ChatService;
import Utils.Statics;
import com.codename1.capture.Capture;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ShareButton;
import com.codename1.components.SpanLabel;
import com.codename1.gif.GifImage;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.Log;
import com.codename1.io.Util;
import com.codename1.l10n.SimpleDateFormat;
import com.codename1.media.Media;
import com.codename1.media.MediaManager;
import com.codename1.messaging.Message;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.CENTER;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import static com.mycompany.myappCo.ListArticleForm.panierArticleStatic;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author omar
 */
public class RecievedMsgsForrm extends BaseForm {

    Resources reserveRes;

    public RecievedMsgsForrm(Resources res, int idSender, int idReciever, Utilisateur user) {

        super("Chatting", BoxLayout.y());
        reserveRes = res;
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Chatting");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
        });

        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, res.getImage("delivery.png"), spacer1, " ", " ", "All Deliveries ");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }
        rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });
         Style sa = UIManager.getInstance().getComponentStyle("Title");
        FontImage icon = FontImage.createMaterial(FontImage.MATERIAL_MIC, sa);

        FileSystemStorage fs = FileSystemStorage.getInstance();
        String recordingsDir = fs.getAppHomePath() + "recordings/";
        fs.mkdir(recordingsDir);

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("ListContact", barGroup);
        all.setUIID("SelectBar2");
        //RadioButton featured = RadioButton.createToggle("livraison", barGroup);
        //featured.setUIID("SelectBar2");
        //RadioButton popular = RadioButton.createToggle("article", barGroup);
        //popular.setUIID("SelectBar2");
        //RadioButton myFavorite = RadioButton.createToggle("technique", barGroup);
        //myFavorite.setUIID("SelectBar2");
        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");

//        add(LayeredLayout.encloseIn(
//                GridLayout.encloseIn(4, all, featured, popular, myFavorite),
//                FlowLayout.encloseBottom(arrow)
//        ));
        //
        ChatService css = ChatService.getInstance();

        Label test = new Label("Write your Msg to " + user.getNom());
        TextArea msgContentField = new TextArea("");
        //
        //GETTING MSGS FROM USER
        System.out.println("Sender ID " + Statics.loggedUser.getIdUtilisateur());
        System.out.println("Reciever ID " + user.getIdUtilisateur());

//
        System.out.println("RECIEVED MSGS ");

        System.out.print(css.getMsgs(idSender, idReciever).size());
//                        if (css.getMsgs(Statics.loggedUser.getIdUtilisateur(), user.getIdUtilisateur()).size() == 0) {
//                            System.out.println("NO MSGS");
//                        } else {
        for (FluxMsg incMsg : css.getMsgs(user.getIdUtilisateur(), Statics.loggedUser.getIdUtilisateur())) {
            System.out.println("MSG CONTENT : " + incMsg.getContent());
            System.out.println("MSG SEEN : " + incMsg.isSeen());

            if (!incMsg.isSeen()) {
                Container mCa = new Container(BoxLayout.x());
                Container recievedCtn = new Container(BoxLayout.y());
                Button markAsSeenBtn = new Button("Seen");
                Image imgMg = res.getImage("user1.png");
                mCa.add(imgMg);
                        
                if( incMsg.getContent().contains("2020")){
                    
                    MultiButton mb = new MultiButton(incMsg.getContent().substring(incMsg.getContent().lastIndexOf("/") + 1));
                  mb.addActionListener((e) -> {
                    try {
                        Media m = MediaManager.createMedia("" + incMsg.getContent(), false);
                        m.play();
                    } catch (IOException err) {
                        Log.e(err);
                    }
                });
                System.out.println("imhereee");
                add(mb);
                 
                }else{
                SpanLabel msgContent = new SpanLabel("Has sent : " + incMsg.getContent());
                mCa.add(msgContent);
                mCa.add(markAsSeenBtn);
                recievedCtn.add(mCa);
                add(recievedCtn);
                    
                }
                markAsSeenBtn.addActionListener(new ActionListener() {

                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        if (css.MsgSeen(incMsg.getIdMsg())) {
                            System.out.println("MSG IS NOW SEEN :" + incMsg.getIdMsg());

                        } else {
                            System.out.println("Error!");
                        }

                    }
                });

            }
        }

//                        }
//                        
        //
        add(test);
        add(msgContentField);
        Button sendMsgBtn = new Button("Send Message");
        add(sendMsgBtn);

        show();

        System.out.println("SELECTED USER " + idReciever);

//                        
        sendMsgBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                FluxMsg msg = new FluxMsg();
                msg.setIdReciever(idReciever);
                msg.setIdSender(idSender);
                msg.setSeen(false);
                msg.setContent(msgContentField.getText().toString());
                boolean result = ChatService.getInstance().SendMsgToUser(msg);
                System.out.println("MSG SENT =" + result);

                //send email
//                                Message m = new Message(""+msgContentField.getText().toString());
//                                m.getAttachments().put("hi", "text/plain");
//                                
//                                Display.getInstance().sendMessage(new String[]{"thelostloki@gmail.com"}, "Message from user"+Statics.loggedUser.getNom(), m);
            }
        });

        ShareButton sb = new ShareButton();
        sb.setText("Partager");

        sb.setTextToShare("Discussion" + msgContentField.getText());

        //String imageFile = FileSystemStorage.getInstance().getAppHomePath() + AffichageArticle.TITREimage;
        add(sb);
            
        
       

        
        
        
        // recording .. not sure abt it
        //setToolbar(new Toolbar());
       
//        try {
//            for (String file : fs.listFiles(recordingsDir)) {
//                MultiButton mb = new MultiButton(file.substring(file.lastIndexOf("/") + 1));
//                mb.addActionListener((e) -> {
//                    try {
//                        Media m = MediaManager.createMedia(recordingsDir + file, false);
//                        m.play();
//                    } catch (IOException err) {
//                        Log.e(err);
//                    }
//                });
//                System.out.println("imhereee");
//                add(mb);
//            }

            getToolbar().addCommandToRightBar("", icon, (ev) -> {
                try {
                    String file = Capture.captureAudio();
                    if (file != null) {
                        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MMM-dd-kk-mm");
                        String fileName = sd.format(new Date());
                        String filePath = recordingsDir + fileName;
                        Util.copy(fs.openInputStream(file), fs.openOutputStream(filePath));
                        MultiButton mb = new MultiButton(fileName);
                        mb.addActionListener((e) -> {
                            try {
                                Media m = MediaManager.createMedia(filePath, false);
                                System.out.println(filePath);
                                m.play();
                            } catch (IOException err) {
                                Log.e(err);
                            }
                        });
                        System.out.println(filePath);
                          FluxMsg msg = new FluxMsg();
                          msg.setIdReciever(idReciever);
                          msg.setIdSender(idSender);
                          msg.setSeen(false);
                          msg.setContent(filePath);
                          boolean result = ChatService.getInstance().SendMsgToUser(msg);
                          System.out.println("MSG SENT =" + result);
                      
                        add(mb);
                        
                        revalidate();
                    }
                
                } catch (IOException err) {
                    Log.e(err);
                }
            });
       

        //
//        sendMsgBtn.addActionListener(new ActionListener() {
//
//            @Override
//            public void actionPerformed(ActionEvent evt) {
//                FluxMsg msg = new FluxMsg();
//                msg.setIdReciever(idReciever);
//                msg.setIdSender(idSender);
//                msg.setContent(msgContentField.getText().toString());
//                boolean result = ChatService.getInstance().SendMsgToUser(msg);
//                System.out.println("MSG SENT =" + result);
//
//                //send email
//                Message m = new Message("" + msgContentField.getText().toString());
//                m.getAttachments().put("hi", "text/plain");
//
//                Display.getInstance().sendMessage(new String[]{"thelostloki@gmail.com"}, "Message from user" + Statics.loggedUser.getNom(), m);
//                msgContentField.setText("");
//            }
//
//        });
        //
    }

    private void addTab(Tabs swipe, Image image, Label spacer1, String likesStr, String commentsStr, String all_Deliveries_) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());

//        if(img.getHeight() < size) {
//            img = img.scaledHeight(size);
//        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
//        }
//        ScaleImageLabel image = new ScaleImageLabel(img);
//        image.setUIID("Container");
//        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
        ScaleImageLabel imgg = new ScaleImageLabel();
        try {
            imgg = new ScaleImageLabel(GifImage.decode(getResourceAsStream("/rec.gif"), 5156565));
        } catch (IOException err) {
            log(err);
        }
        imgg.setUIID("Container");
        imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
//                imgg.getAllStyles().setBorder(Border.createEmpty());
//                imgg.getAllStyles().setBgTransparency(255);
        Container page1
                = LayeredLayout.encloseIn(
                        imgg,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY( //                            new SpanLabel(text, "LargeWhiteText"),
                                //                            FlowLayout.encloseIn(likes, comments),
                                //                            spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }
}
