/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package com.mycompany.myappCo;

import Entites.Article;
import Entites.Categorie;
import Entites.Societe;
import Services.ArticleService;
import Services.FavorisService;
import Services.FilterService;
import Services.SocieteService;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.codescan.CodeScanner;
import com.codename1.codescan.ScanResult;
import com.codename1.components.ImageViewer;
import com.codename1.components.InfiniteProgress;
import com.codename1.components.MultiButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.gif.GifImage;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.CENTER;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author Letaief Sofiene
 */
public class ListArticleForm extends BaseForm {

    Resources reserveRes;
    Container cntAll = new Container();

    public static ArrayList<Article> displayArticles;
    ArrayList<Article> panierArticle;
    //STATIC PANIER
    static ArrayList<Article> panierArticleStatic;

    public ListArticleForm(Resources res, Categorie cat) {

        super("", BoxLayout.y());

        reserveRes = res;
        cntAll.setScrollableY(true);
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("ListArticle");
        getContentPane().setScrollVisible(false);
        getContentPane().setScrollableX(false);
        getContentPane().setScrollableY(false);
         SpanLabel NoDisplay =new SpanLabel(" No Resilts For ");
        NoDisplay.setUIID("url");
        NoDisplay.setHidden(true);
        NoDisplay.setVisible(false);
        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
            String text = (String) e.getSource();
            if (text == null || text.length() == 0) {

                // clear search
                for (Component cmp : cntAll) {
                    cmp.setHidden(false);
                    cmp.setVisible(true);
                }
                 NoDisplay.setHidden(true);
                 NoDisplay.setVisible(false);

            } else {
                text = text.toLowerCase();
                int x = 0;

                for (Component cmp : cntAll) {

                    String line1 = cmp.getName();

                    boolean show = line1 != null && line1.toLowerCase().indexOf(text) > -1;
                    cmp.setHidden(!show);
                    cmp.setVisible(show);
                    if (show == true) {
                        x++;
                    };
                }
                if(x==0){
                         NoDisplay.setText("No Results for "+ (String) e.getSource());
                         NoDisplay.setHidden(false);
                         NoDisplay.setVisible(true);
                }else{
                         NoDisplay.setHidden(true);
                         NoDisplay.setVisible(false);
                
                }

                cntAll.animateLayout(150);
            }
        }, 4);
        EncodedImage enc = EncodedImage.createFromImage(res.getImage("loading.jpg"), true);

        Dialog d = new Dialog("Trier par ");
        d.setLayout(new BorderLayout());
        Label prixbas = new Label("Prix le plus bas");
        Label prixeleve = new Label("Prix le plus éleves");
        Label favoris = new Label("les plus aimées");
        Label nomAsc = new Label("alphabet Z-A");
        Label nomDesc = new Label("alphabet A-Z ");
        favoris.setUIID("Label3");
        prixeleve.setUIID("Label3");
        nomAsc.setUIID("Label3");
        nomDesc.setUIID("Label3");
        prixbas.setUIID("Label3");

        favoris.setTextPosition(LEFT);
        prixeleve.setTextPosition(LEFT);
        nomAsc.setTextPosition(LEFT);
        nomDesc.setTextPosition(LEFT);
        prixbas.setTextPosition(LEFT);
        Style checkStyle = new Style(nomAsc.getUnselectedStyle());
        checkStyle.setFgColor(0xff2d55);
        FontImage checkImage = FontImage.createMaterial(FontImage.MATERIAL_CHECK, checkStyle);

        prixbas.addPointerPressedListener(l -> {
            cntAll.removeAll();
            prixbas.setIcon(checkImage);
            prixeleve.setIcon(null);
            favoris.setIcon(null);
            nomAsc.setIcon(null);
            nomDesc.setIcon(null);

            sortByPrixAsc();
            for (Article ar : displayArticles) {
                final String url3 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url3);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            d.dispose();
            cntAll.animateLayout(150);
        });

        prixeleve.addPointerPressedListener(l -> {
            cntAll.removeAll();
            prixbas.setIcon(null);
            prixeleve.setIcon(checkImage);
            favoris.setIcon(null);
            nomAsc.setIcon(null);
            nomDesc.setIcon(null);
            sortByPrixDesc();
            for (Article ar : displayArticles) {
                final String url3 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url3);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            d.dispose();
            cntAll.animateLayout(150);
        });

        nomAsc.addPointerPressedListener(l -> {
            cntAll.removeAll();
            prixbas.setIcon(null);
            prixeleve.setIcon(null);
            favoris.setIcon(null);
            nomAsc.setIcon(checkImage);
            nomDesc.setIcon(null);
            sortByNomDesc();
            for (Article ar : displayArticles) {
                final String url3 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url3);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            d.dispose();
            cntAll.animateLayout(150);
        });

        nomDesc.addPointerPressedListener(l -> {
            cntAll.removeAll();
            prixbas.setIcon(null);
            prixeleve.setIcon(null);
            favoris.setIcon(null);
            nomAsc.setIcon(null);
            nomDesc.setIcon(checkImage);
            sortByNomAsc();
            for (Article ar : displayArticles) {
                final String url3 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url3);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            d.dispose();
            cntAll.animateLayout(150);
        });

        favoris.addPointerPressedListener(l -> {
            cntAll.removeAll();
            prixbas.setIcon(null);
            prixeleve.setIcon(null);
            favoris.setIcon(checkImage);
            nomAsc.setIcon(null);
            nomDesc.setIcon(null);
            sortByFav();
            for (Article ar : displayArticles) {
                final String url3 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url3);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            d.dispose();
            cntAll.animateLayout(150);
        });

        Dialog.setDefaultBlurBackgroundRadius(8);
        d.add(BorderLayout.CENTER, BoxLayout.encloseY(nomDesc, nomAsc, prixbas, prixeleve, favoris));

//        d.show(getContentPane().getHeight() / 2, 0, 0, 0);
        panierArticleStatic = new ArrayList<Article>();
        
        
        Tabs swipe = new Tabs();
        swipe.setScrollableY(true);

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, " ", " ", "Open Quantumm ");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }

        rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });
        radioContainer.setScrollableY(true);

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("All", barGroup);
        all.setUIID("SelectBar2");

        RadioButton myFavorite = RadioButton.createToggle("My Favorites", barGroup);
        myFavorite.setUIID("SelectBar2");

        RadioButton filter = RadioButton.createToggle("Filter", barGroup);
        filter.setUIID("SelectBar2");

        RadioButton ScanQr = RadioButton.createToggle("ScanQr", barGroup);
        ScanQr.setUIID("SelectBar2");

        RadioButton trierPar = RadioButton.createToggle("Trier Par", barGroup);
        trierPar.setUIID("SelectBar2");
        trierPar.addActionListener(l -> {
            d.show(getContentPane().getHeight() / 2, 0, 0, 0);

        });
            
        myFavorite.addActionListener(l->{
        cntAll.removeAll();
          ArrayList<Article> favList=new ArrayList<>(  FavorisService.getInstance().getListArticleFavoris());
            for (Article ar : favList) {
                final String url5 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url5);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            cntAll.animateLayout(150);
        
        });
        
           all.addActionListener(l->{
        cntAll.removeAll();
          
            for (Article ar : displayArticles) {
                final String url6 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url6);
                addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
            }
            cntAll.animateLayout(150);
        
        });
        
        
        
        
        
        ScanQr.addActionListener(l -> {
            CodeScanner.getInstance().scanQRCode(new ScanResult() {

                public void scanCompleted(String contents, String formatName, byte[] rawBytes) {
                    //barCode.setText("Bar: " + contents);
                    Article articleQr = ArticleService.getInstance().getArticleFromId(Integer.parseInt(contents));
                    if (articleQr == null) {
                        ToastBar.showMessage("this is not our QrCode Sorry !", FontImage.MATERIAL_FAVORITE, 4000);
                    } else {
                        new DetailsArticleForm(reserveRes, articleQr).show();
                    }
                }

                public void scanCanceled() {
                    System.out.println("cancelled");
                }

                public void scanError(int errorCode, String message) {
                    System.out.println("err " + message);
                }
            });

        });

        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");
        //change arrow
        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(5, all, myFavorite, trierPar, filter, ScanQr),
                FlowLayout.encloseBottom(arrow)
        ));

        all.setSelected(true);
        arrow.setVisible(false);
        addShowListener(e -> {
            arrow.setVisible(true);
            updateArrowPosition(all, arrow);
        });
        bindButtonSelection(all, arrow);
        bindButtonSelection(trierPar, arrow);
        bindButtonSelection(myFavorite, arrow);
        bindButtonSelection(filter, arrow);
        bindButtonSelection(ScanQr, arrow);

        // special case for rotation
        addOrientationListener(e -> {
            updateArrowPosition(barGroup.getRadioButton(barGroup.getSelectedIndex()), arrow);
        });

        String url;

        FilterService fs = FilterService.getInstance();

        for (Article ar : displayArticles) {
            url = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
//            URLImage urlImg = URLImage.createToStorage(enc, ar.getDesignation()+ar.getIdArticle(), url);
            URLImage urlImg = URLImage.createToStorage(enc, ar.getImage(), url);
            addButton(urlImg, ar.getDesignation(), false, ar.getNbLike(), ar.getNbReview(), ar.getIdArticle(), ar);
        }
       

        
        add(cntAll);
        
       add(NoDisplay);

//      
    }

    public void sortByNomDesc() {
        Collections.sort(displayArticles, (Article h1, Article h2) -> h2.getDesignation().compareTo(h1.getDesignation()));
    }

    public void sortByNomAsc() {
        Collections.sort(displayArticles, (Article h1, Article h2) -> h1.getDesignation().compareTo(h2.getDesignation()));
    }

    public void sortByPrixDesc() {
        Collections.sort(displayArticles, (Article h1, Article h2) -> h2.getPrixVente() - h1.getPrixVente());
    }

    public void sortByPrixAsc() {
        Collections.sort(displayArticles, (Article h1, Article h2) -> h1.getPrixVente() - h2.getPrixVente());
    }

    public void sortByFav() {
        Collections.sort(displayArticles, (Article h1, Article h2) -> h2.getNbLike() - h1.getNbLike());
    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();

    }

    private void addTab(Tabs swipe, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
//        if(img.getHeight() < size) {
//            img = img.scaledHeight(size);
//        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
//        }
//        ScaleImageLabel image = new ScaleImageLabel(img);
//        image.setUIID("Container");
//        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
        ScaleImageLabel imgg = new ScaleImageLabel();
        try {
            imgg = new ScaleImageLabel(GifImage.decode(getResourceAsStream("/testt.gif"), 5156565));
        } catch (IOException err) {
            log(err);
        }
        imgg.setUIID("Container");
        imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
//                imgg.getAllStyles().setBorder(Border.createEmpty());
//                imgg.getAllStyles().setBgTransparency(255);
        Container page1
                = LayeredLayout.encloseIn(
                        imgg,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY( //                            new SpanLabel(text, "LargeWhiteText"),
                                //                            FlowLayout.encloseIn(likes, comments),
                                //                            spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }

    private void addButton(Image img, String title, boolean liked, int likeCount, int commentCount, int idSoc, Article article) {
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);

        Button image = new Button(img.fill(width, height));
        image.setUIID("Label");

        Container cnt = BorderLayout.west(image);
        cnt.setName(title + article.getIdModele().getLibelle() + article.getModele().getLibelle() + article.getIdModele().getIdConstructeur().getLibelle() + article.getPrixVente());
        System.out.println(cnt.getName());
////        cnt.setLeadComponent(image);
//"Constructeur:"+article.getIdModele().getIdConstructeur().getLibelle()+" Modele:"+article.getIdModele().getLibelle()
        TextArea ta = new TextArea(" " + title + "\n Price:" + article.getPrixVente() + "$\n Constructeur:" + article.getIdModele().getIdConstructeur().getLibelle() + "\n Modele:" + article.getIdModele().getLibelle());
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(likeCount + " Likes  ", "NewsBottomLine");
        likes.setTextPosition(RIGHT);

        Label likes2 = new Label(likeCount + " Likes  ", "NewsBottomLine");

//        if (!liked) {
//            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
//        } else {
        Style s = new Style(likes.getUnselectedStyle());
        s.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
        likes.setIcon(heartImage);
        Label likeBtn = new Label(" ");

//        }
        Label comments = new Label(commentCount + " Review", "NewsBottomLine");
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);

        Button addToCartBtn = new Button("Details");
        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        BoxLayout.encloseX(ta),
                        //                        BoxLayout.encloseX(ta2),
                        //                        BoxLayout.encloseX(ta3),
                        BoxLayout.encloseX(likes, comments)
                ));

        cnt.add(BorderLayout.EAST, BoxLayout.encloseY(likeBtn, addToCartBtn));

        cntAll.add(cnt);
//       add(cnt);
//       cnt.removeAll();

        addToCartBtn.addActionListener(e -> {

            new DetailsArticleForm(reserveRes, article).show();
        });
//
////       image.addActionListener(e -> ToastBar.showMessage(title, FontImage.MATERIAL_INFO));
//        image.addActionListener(e -> {
//
//            System.out.println("Button pressed");
//
//            System.out.println("Articled ID : " + article.getIdArticle());
//           panierArticle.add(article);
//            System.out.println("Current nb article in panier : " + panierArticle.size());
//            panierArticleStatic = panierArticle;
//
//        });
    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);

            }
        });
    }

    public static void setDisplayArticles(Categorie cat) {
        FilterService fs = FilterService.getInstance();
        displayArticles = fs.filterByCategorie(cat);

    }
}
