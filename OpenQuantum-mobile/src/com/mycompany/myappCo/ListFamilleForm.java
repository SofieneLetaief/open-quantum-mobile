/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package com.mycompany.myappCo;

import Entites.Famille;
import Entites.FluxMsg;
import Entites.Utilisateur;
import Services.ArticleService;
import Services.ChatService;
import Services.FamilleService;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.gif.GifImage;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.CENTER;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class ListFamilleForm extends BaseForm {

    Resources reserveRes;
    int selectedUserId = 0;

    public ListFamilleForm(Resources res) {

        super("", BoxLayout.y());

        reserveRes = res;
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("OpenQuantum");
        getContentPane().setScrollVisible(false);
//            getContentPane().setScrollableX(false);
//                   getContentPane().setScrollableY(false);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });

        tb.getAllStyles().setBorder(Border.createEmpty());
        tb.getAllStyles().setBackgroundType(Style.BACKGROUND_NONE);
        tb.getAllStyles().setBgTransparency(255);
        tb.getAllStyles().setBgColor(0x99CCCC);
        Tabs swipe = new Tabs();
        Label spacer1 = new Label();
        Label spacer2 = new Label();
        Container c = new Container(new FlowLayout(CENTER));
        c.setUIID("Container");
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        Component.setSameSize(radioContainer, spacer1, spacer2);
//      add(LayeredLayout.encloseIn(swipe));
        Container Style = new Container(BoxLayout.y());
        Label test = new Label("hello");
        Label test2 = new Label("hello");
        Label test3 = new Label("hello");
        
        test2.getAllStyles().setFgColor(0xFFFFFF);
        test.getAllStyles().setFgColor(0xFFFFFF);
        test3.getAllStyles().setFgColor(0xFFFFFF);
        
        Style.add(test);
        Style.add(test2);
        Style.add(test3);
        
        Style.setScrollableY(false);
    
        Container LF = new Container(new GridLayout(8, 1));
        LF.setScrollableY(true);
        FamilleService fs = FamilleService.getInstance();
        System.out.println(fs.getFamilles());
        for (Famille fam : fs.getFamilles()) {
            Button btn = new Button(fam.getLibelle());
            btn.setUIID("Button2");
         
            btn.addActionListener(l->{
             new ListCategorieForm(reserveRes,fam).show();
            });
         
            LF.add(btn);

        }


        Container MidCont = FlowLayout.encloseCenterMiddle();
        Label l = new Label("List Collections");
        l.setUIID("Label2");

        MidCont.add(l);
        add(Style);
        add(MidCont);
        add(LF);

    }

  
    

  
}
