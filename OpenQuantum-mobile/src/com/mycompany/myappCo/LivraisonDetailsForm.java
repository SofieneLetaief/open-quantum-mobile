/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Livraison;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.components.InteractionDialog;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ToastBar;
import com.codename1.gif.GifImage;
import com.codename1.googlemaps.MapContainer;
import com.codename1.maps.Coord;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Rectangle;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import java.io.IOException;

/**
 *
 * @author HP OMEN
 */
public class LivraisonDetailsForm extends BaseForm {

    private Resources livraisonTheme;

    public LivraisonDetailsForm(Resources res, Livraison livraison) {

//        super("Livraison", BoxLayout.y());
//        livraisonTheme = res;
//        Toolbar tb = new Toolbar(true);
//
//        setToolbar(tb);
//        getTitleArea().setUIID("Container");
//        getContentPane().setScrollVisible(false);
        super("Livraison Details", BoxLayout.y());
        livraisonTheme = res;
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Livraison "+livraison.getIdLivraison()+" Details");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
        });

        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, res.getImage("delivery.png"), spacer1, " ", " ", "All Deliveries ");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }

        rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("Livraison Summary", barGroup);
        all.setUIID("SelectBarDelivery");

        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");

        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(4, all),
                FlowLayout.encloseBottom(arrow)
        ));

        all.setSelected(true);
        arrow.setVisible(false);
        addShowListener(e -> {
            arrow.setVisible(true);
            updateArrowPosition(all, arrow);
        });
        bindButtonSelection(all, arrow);

        //DETAILS HERE
        Label livraisonDetailsLb = new Label("Livraison Details : ");
        Label trackingcodeLivraisonLb = new Label("Tracking Code :" + livraison.getTrackingCode());

        Container detailsCtn = new Container(BoxLayout.y());
        detailsCtn.addAll(livraisonDetailsLb, trackingcodeLivraisonLb);

     
        String url;
        EncodedImage enc = EncodedImage.createFromImage(res.getImage("loading.jpg"), true);

        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);
        url = "http://localhost/open-quantum-web/web/Uploads/Images/" + livraison.getLivreur().getImage();
        System.out.println(url);
        URLImage urlImg = URLImage.createToStorage(enc, "ok" + livraison.getLivreur().getImage(), url);
        Button image = new Button(urlImg.fill(width, height));
        image.setUIID("Label");
        detailsCtn.add(image);
        
           add(detailsCtn);
           
             Button mapBtn = new Button("Open Map");
             
             mapBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
              
                
                  Form hi = new Form("Native Maps Test");
                hi.setLayout(new BorderLayout());
                final MapContainer cnt = new MapContainer("AIzaSyBwIiYB7YyTIC23q7YdBkKSoyIIPAja-vc");
//                Location lastKnownLocation = LocationManager.getLocationManager().getLastKnownLocation();
//                Style ss = new Style();
//                ss.setBgTransparency(0);
//                ss.setFgColor(0);
//                cnt.addMarker(FontImage.createMaterial(FontImage.MATERIAL_MY_LOCATION, ss).toEncodedImage(), new Coord(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()), "", "", rrr -> {
//                    ToastBar.showErrorMessage(lastKnownLocation.toString());
//                });
               Button goBackBtn = new Button("Bk");
               
               goBackBtn.addActionListener(new ActionListener() {

                      @Override
                      public void actionPerformed(ActionEvent evt) {
                            new LivraisonForm(livraisonTheme).showBack();
                      }
                  });

                Button btnMoveCamera = new Button("Move Camera");
                btnMoveCamera.addActionListener(e -> {
                    cnt.setCameraPosition(new Coord(-33.867, 151.206));
                });
                Style s = new Style();
                s.setFgColor(0xff0000);
                s.setBgTransparency(0);
                FontImage markerImg = FontImage.createMaterial(FontImage.MATERIAL_PLACE, s, Display.getInstance().convertToPixels(3));

                Button btnAddMarker = new Button("Add Marker");
                btnAddMarker.addActionListener(e -> {

                    cnt.setCameraPosition(new Coord(41.889, -87.622));
                    cnt.addMarker(
                            EncodedImage.createFromImage(markerImg, false),
                            cnt.getCameraPosition(),
                            "Hi marker",
                            "Optional long description",
                            ttt -> {
                                ToastBar.showMessage("You clicked the marker", FontImage.MATERIAL_PLACE);
                            }
                    );

                });

                Button btnAddPath = new Button("Add Path");
                btnAddPath.addActionListener(e -> {

                    cnt.addPath(
                            cnt.getCameraPosition(),
                            new Coord(-33.866, 151.195), // Sydney
                            new Coord(-18.142, 178.431), // Fiji
                            new Coord(21.291, -157.821), // Hawaii
                            new Coord(37.423, -122.091) // Mountain View
                    );
                });

                Button btnClearAll = new Button("Clear All");
                btnClearAll.addActionListener(e -> {
                    cnt.clearMapLayers();
                });

                cnt.addTapListener(e -> {
                    TextField enterName = new TextField();
                    Container wrapper = BoxLayout.encloseY(new Label("Name:"), enterName);
                    InteractionDialog dlg = new InteractionDialog("Add Marker");
                    dlg.getContentPane().add(wrapper);
                    enterName.setDoneListener(e2 -> {
                        String txt = enterName.getText();
                        cnt.addMarker(
                                EncodedImage.createFromImage(markerImg, false),
                                cnt.getCoordAtPosition(e.getX(), e.getY()),
                                enterName.getText(),
                                "",
                                e3 -> {
                                    ToastBar.showMessage("You clicked " + txt, FontImage.MATERIAL_PLACE);
                                }
                        );
                        dlg.dispose();
                    });
                    dlg.showPopupDialog(new Rectangle(e.getX(), e.getY(), 10, 10));
                    enterName.startEditingAsync();
                });

                Container root = LayeredLayout.encloseIn(
                        BorderLayout.center(cnt),
                        BorderLayout.south(
                                FlowLayout.encloseBottom(goBackBtn,btnMoveCamera, btnAddMarker, btnAddPath, btnClearAll)
                        )
                );

                hi.add(BorderLayout.CENTER, root);
                hi.show();

           
            }
        });
             
             add(mapBtn);
             
    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();

    }

    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
//        if(img.getHeight() < size) {
//            img = img.scaledHeight(size);
//        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
//        }
//        ScaleImageLabel image = new ScaleImageLabel(img);
//        image.setUIID("Container");
//        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
        ScaleImageLabel imgg = new ScaleImageLabel();
        try {
            imgg = new ScaleImageLabel(GifImage.decode(getResourceAsStream("/servicerapide.gif"), 5156565));
        } catch (IOException err) {
            log(err);
        }
        imgg.setUIID("Container");
        imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        Container page1
                = LayeredLayout.encloseIn(
                        imgg,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY( //                            new SpanLabel(text, "LargeWhiteText"),
                                //                            FlowLayout.encloseIn(likes, comments),
                                //                            spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }

    private void addButton(Image img, String title, boolean liked, String trackingcode, int commentCount, int idSoc, Livraison liv) {
        Livraison livraison = liv;
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);
        Button image = new Button(img.fill(width, height));
        image.setUIID("Label");
        Container cnt = BorderLayout.west(image);
        cnt.setLeadComponent(image);
        TextArea ta = new TextArea(title);
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(trackingcode + "", "NewsBottomLine");
        likes.setTextPosition(RIGHT);
        if (!liked) {
            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
        } else {
            Style s = new Style(likes.getUnselectedStyle());
            s.setFgColor(0xff2d55);
            FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
            likes.setIcon(heartImage);
        }
        Label comments = new Label(commentCount + " Comments", "NewsBottomLine");
        FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);

        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        ta,
                        BoxLayout.encloseX(likes, comments)
                ));
        add(cnt);
//       image.addActionListener(e -> ToastBar.showMessage(title, FontImage.MATERIAL_INFO));
        image.addActionListener(e -> {
            //

        });
    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }

}
