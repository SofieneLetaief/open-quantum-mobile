/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Livraison;
import Services.LivraisonService;
import Utils.CurrentSession;
import Utils.Statics;
import static Utils.Statics.accountSID;
import static Utils.Statics.authToken;
import static Utils.Statics.fromPhone;
import com.codename1.charts.ChartComponent;
import com.codename1.charts.models.CategorySeries;
import com.codename1.charts.renderers.DefaultRenderer;
import com.codename1.charts.renderers.SimpleSeriesRenderer;
import com.codename1.charts.util.ColorUtil;
import com.codename1.charts.views.PieChart;
import com.codename1.codescan.CodeScanner;
import com.codename1.codescan.ScanResult;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.gif.GifImage;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.util.Base64;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author HP OMEN
 */
public class LivraisonForm extends BaseForm {

    private Resources livraisonTheme;

    public LivraisonForm(Resources res) {

//        super("Livraison", BoxLayout.y());
//        livraisonTheme = res;
//        Toolbar tb = new Toolbar(true);
//
//        setToolbar(tb);
//        getTitleArea().setUIID("Container");
//        getContentPane().setScrollVisible(false);
        super("Livraison", BoxLayout.y());
        livraisonTheme = res;
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Livraison");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
        });

        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, res.getImage("delivery.png"), spacer1, " ", " ", "All Deliveries ");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }

        rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("All", barGroup);
        all.setUIID("SelectBarDelivery");
        RadioButton featured = RadioButton.createToggle("Scan QR", barGroup);
        featured.setUIID("SelectBarDelivery");
        RadioButton popular = RadioButton.createToggle("Summary", barGroup);
        popular.setUIID("SelectBarDelivery");
        RadioButton myFavorite = RadioButton.createToggle("My Favorites", barGroup);
        myFavorite.setUIID("SelectBarDelivery");
        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");

        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(4, all, featured, popular, myFavorite),
                FlowLayout.encloseBottom(arrow)
        ));

        all.setSelected(true);
        arrow.setVisible(false);
        addShowListener(e -> {
            arrow.setVisible(true);
            updateArrowPosition(all, arrow);
        });
        bindButtonSelection(all, arrow);
        bindButtonSelection(featured, arrow);
        bindButtonSelection(popular, arrow);
        bindButtonSelection(myFavorite, arrow);

        //SCAN QR CODE
        Container cnt = new Container(BoxLayout.y());

        featured.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                CodeScanner.getInstance().scanQRCode(new ScanResult() {

                    public void scanCompleted(String contents, String formatName, byte[] rawBytes) {
                        //barCode.setText("Bar: " + contents);
                        Label resultLb = new Label("Your QR Code scan result: ");
                        cnt.add(resultLb);
                        cnt.addComponent(new Label(contents));
                        add(cnt);
                        cnt.revalidate();
                    }

                    public void scanCanceled() {
                        System.out.println("cancelled");
                    }

                    public void scanError(int errorCode, String message) {
                        System.out.println("err " + message);
                    }
                });
            }
        });
        //
        // special case for rotation
        addOrientationListener(e -> {
            updateArrowPosition(barGroup.getRadioButton(barGroup.getSelectedIndex()), arrow);
        });

        String url;
        EncodedImage enc = EncodedImage.createFromImage(res.getImage("loading.jpg"), true);

//             LivraisonService ls=LivraisonService.getInstance();
//        for (Livraison liv : ls.getLivraisons(23)) { 	
//            url="http://localhost/open-quantum-web/web/Uploads/Images/"+soc.getLogo();
//           
//              URLImage urlImg = URLImage.createToStorage(enc, soc.getRaisonSociale()+soc.getIdSociete(), url);
//              
//         
//           addButton(urlImg, soc.getRaisonSociale(), true, 26, 32,soc.getIdSociete());	
//      }
//        
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//      Response<Map> result = Rest.post("https://api.twilio.com/2010-04-01/Accounts/" + accountSID + "/Messages.json").
//        queryParam("To", "+21696250615").
//        queryParam("From", fromPhone).
//        queryParam("Body", "this is a test").
//        header("Authorization", "Basic " + Base64.encodeNoNewline((accountSID + ":" + authToken).getBytes())).
//        getAsJsonMap();;
//        //////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
//////////////////////////////            }MATNA7IIIICH EL CMTTT
        LivraisonService ls = LivraisonService.getInstance();
        System.out.println(ls.getLivraisons(Statics.loggedUser.getIdSociete()));
        System.out.println("User id societe" + Statics.loggedUser.getIdSociete());
        int fixImgIndex = 0;
        for (Livraison liv : ls.getLivraisons(Statics.loggedUser.getIdSociete())) {
            liv.setIdUtilisateur(Statics.loggedUser.getIdUtilisateur());
            System.out.println("Displaying livraisons : ");
            System.out.println(liv.toString());
            url = Statics.LIVE_URL + "/open-quantum-web/web/Uploads/Images/" + liv.getLivreur().getImage();
            System.out.println(url);
            URLImage urlImg = URLImage.createToStorage(enc, "ok" + liv.getLivreur().getImage(), url);
            //System.out.println("url img"+urlImg);
            addButton(urlImg, liv.getStatus(), true, liv.getTrackingCode(), 32, CurrentSession.id_societe, liv);
        }

        int currentNbLivraison = 0;

        currentNbLivraison = ls.getLivraisons(Statics.loggedUser.getIdSociete()).size();
        final int currentNbLivraisons = currentNbLivraison;
        ArrayList<Livraison> actualLivraisons = ls.getLivraisons(Statics.loggedUser.getIdSociete());
        Collection actualLivraisonsCollection = actualLivraisons;
        //THREAD HERE TEST NEW CHANGES !!
        Timer t = new Timer();

        t.schedule(new TimerTask() {
            ArrayList<Livraison> tempLivraisons;

            @Override
            public void run() {

                System.out.println("-------AUTO APP UPDATE---------");
                tempLivraisons = ls.getLivraisons(Statics.loggedUser.getIdSociete());
                Collection DifftempLivraisonsCollection = tempLivraisons;
                DifftempLivraisonsCollection.removeAll(actualLivraisonsCollection);
                //
                System.out.println("Diff: " + DifftempLivraisonsCollection);
                System.out.println("Diff SIZE: " + DifftempLivraisonsCollection.size());

                //
                int incmNbLivraison = DifftempLivraisonsCollection.size();

//                  if(currentNbLivraisons!=incmNbLivraison){
//                     
//                      System.out.println("NEW CHANGES");
//                      new LivraisonForm(livraisonTheme).show();
//                  }
                //LOGICS
                System.out.println("Actual Nb Livraisons : " + actualLivraisons.size() + " Incoming Nb Livraison :" + tempLivraisons.size());

            }
        }, 0, 5000);

        //
        //
        //chart code heree
        // Generate the values
        double[] values = new double[]{12, 14, 11, 10, 19};

        // Set up the renderer
        int[] colors = new int[]{ColorUtil.BLUE, ColorUtil.GREEN, ColorUtil.MAGENTA, ColorUtil.YELLOW, ColorUtil.CYAN};
        DefaultRenderer renderer = buildCategoryRenderer(colors);
        renderer.setZoomButtonsVisible(true);
        renderer.setZoomEnabled(true);
        renderer.setChartTitleTextSize(20);
        renderer.setDisplayValues(true);
        renderer.setShowLabels(true);
        SimpleSeriesRenderer r = renderer.getSeriesRendererAt(0);
        r.setGradientEnabled(true);
        r.setGradientStart(0, ColorUtil.BLUE);
        r.setGradientStop(0, ColorUtil.GREEN);
        r.setHighlighted(true);

        // Create the chart ... pass the values and renderer to the chart object.
        ArrayList<Livraison> livraisons = ls.getLivraisons(Statics.loggedUser.getIdSociete());

        PieChart chart = new PieChart(buildCategoryDataset("Livraison Summary", livraisons), renderer);

        // Wrap the chart in a Component so we can add it to a form
        ChartComponent c = new ChartComponent(chart);

        popular.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                try {
                    add(c);
                } catch (Exception ex) {
                    System.out.println("Catched Chart Exception!");
                }
                t.cancel();
            }
        });

        //
        int deliveredNbLiv = 0, UnderdeliveryNbLiv = 0;
        for (Livraison liv : livraisons) {
            if (liv.getStatus().equals("Not delivered")) {
                UnderdeliveryNbLiv++;
            } else if (liv.getStatus().equals("Delivered")) {
                deliveredNbLiv++;
            }
        }

        Label deliveredNb = new Label("Delivered Livraisons : " + deliveredNbLiv);
        Label UnderdeliveryNb = new Label("Currently Under Delivery: " + UnderdeliveryNbLiv);

        Container statsLivCtn = new Container(BoxLayout.y());
        statsLivCtn.addAll(deliveredNb, UnderdeliveryNb);

        add(statsLivCtn);

    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();

    }

    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
//        if(img.getHeight() < size) {
//            img = img.scaledHeight(size);
//        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
//        }
//        ScaleImageLabel image = new ScaleImageLabel(img);
//        image.setUIID("Container");
//        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
        ScaleImageLabel imgg = new ScaleImageLabel();
        try {
            imgg = new ScaleImageLabel(GifImage.decode(getResourceAsStream("/delivery.gif"), 5156565));
        } catch (IOException err) {
            log(err);
        }
        imgg.setUIID("Container");
        imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        Container page1
                = LayeredLayout.encloseIn(
                        imgg,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY( //                            new SpanLabel(text, "LargeWhiteText"),
                                //                            FlowLayout.encloseIn(likes, comments),
                                //                            spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }

    private void addButton(Image img, String title, boolean liked, String trackingcode, int commentCount, int idSoc, Livraison liv) {
        Livraison livraison = liv;
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);
        Button image = new Button(img.fill(width, height));
        image.setUIID("Label");
        Container cnt = BorderLayout.west(image);
        cnt.setLeadComponent(image);
        TextArea ta = new TextArea(title);
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(trackingcode + "", "NewsBottomLine");
        likes.setTextPosition(RIGHT);
        if (!liked) {
            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
        } else {
            Style s = new Style(likes.getUnselectedStyle());
            s.setFgColor(0xff2d55);
            FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
            likes.setIcon(heartImage);
        }
        Label statusLb = new Label("", "NewsBottomLine");
        if (liv.getStatus().equals("Delivered")) {
            statusLb.setText("Arrived");
            statusLb.setUIID("ArrivedLabel");

        } else {
            statusLb.setText("On The Way");
            statusLb.setUIID("OrangeLabel");

        }

        FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);

        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        ta,
                        BoxLayout.encloseX(likes, statusLb)
                ));
        add(cnt);
//       image.addActionListener(e -> ToastBar.showMessage(title, FontImage.MATERIAL_INFO));
        image.addActionListener(e -> {
            //SELECTED LIVRAISON WORK HERE
            //PRITING LIVRAION HERE
            System.out.println("Swapping to Livraison reclamation");

            new LivraisonDetailsForm(livraisonTheme, livraison).show();

        });
    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }

    //CHARTS HERE
    /**
     * Creates a renderer for the specified colors.
     */
    private DefaultRenderer buildCategoryRenderer(int[] colors) {
        DefaultRenderer renderer = new DefaultRenderer();
        renderer.setLabelsTextSize(38);
        renderer.setLegendTextSize(38);
        renderer.setLabelsColor(0);
        renderer.setMargins(new int[]{20, 30, 15, 0});
        for (int color : colors) {
            SimpleSeriesRenderer r = new SimpleSeriesRenderer();
            r.setColor(color);
            renderer.addSeriesRenderer(r);
        }
        return renderer;
    }

    /**
     * Builds a category series using the provided values.
     *
     * @param titles the series titles
     * @param values the values
     * @return the category series
     */
    protected CategorySeries buildCategoryDataset(String title, ArrayList<Livraison> livraisons) {
        CategorySeries series = new CategorySeries(title);
        int k = 0;
        for (Livraison liv : livraisons) {
            if (liv.getStatus().equals("Delivered")) {
                series.add("Livraison " + liv.getLivreur().getNom() + "[Delivered!]", liv.getIdLivraison());
            } else {
                series.add("Livraison " + liv.getLivreur().getNom() + "[Not Delivered!]", liv.getIdLivraison());

            }

        }
        return series;

    }

}
