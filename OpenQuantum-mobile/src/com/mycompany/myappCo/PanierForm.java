/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Article;
import Entites.Commande;
import Entites.Livraison;
import Entites.Promotion;
import Services.CommandeService;
import Services.LigneCommandeService;
import Services.PromotionService;
import Utils.Statics;
import static Utils.Statics.accountSID;
import static Utils.Statics.authToken;
import static Utils.Statics.fromPhone;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.gif.GifImage;
import com.codename1.io.rest.Response;
import com.codename1.io.rest.Rest;
import com.codename1.messaging.Message;
import static com.codename1.push.PushContent.setTitle;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.util.Base64;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author med talel
 */
public class PanierForm extends BaseForm {

    private Resources PanierTheme;
    static ArrayList<Article> currentPanier;
    Container cntAll = new Container();
    static public HashMap<Article, Integer> panierMap = new HashMap<Article, Integer>();
    float prixTotal = 0;
    float totaldis = 0;

    public PanierForm(Resources res, ArrayList<Article> incomingPanier) {

        super("panier", BoxLayout.y());
        PanierTheme = res;
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Panier");
        getContentPane().setScrollVisible(false);

        super.addSideMenu(res);
        tb.addSearchCommand(e -> {
        });

        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, res.getImage("delivery.png"), spacer1, " ", " ", "All Deliveries ");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();

        ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }
        rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if (!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });

        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));

        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("All", barGroup);
        all.setUIID("SelectBar2");
        RadioButton featured = RadioButton.createToggle("Featured", barGroup);
        featured.setUIID("SelectBar2");
        RadioButton popular = RadioButton.createToggle("Popular", barGroup);
        popular.setUIID("SelectBar2");
        RadioButton myFavorite = RadioButton.createToggle("My Favorites", barGroup);
        myFavorite.setUIID("SelectBar2");
        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");

        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(4, all, featured, popular, myFavorite),
                FlowLayout.encloseBottom(arrow)
        ));
        String url;
        EncodedImage enc = EncodedImage.createFromImage(res.getImage("loading.jpg"), true);
//
//        for (Article ar : panierList) {
//            url = "http://localhost/open-quantum-web/web/Uploads/Images/" + ar.getImage();
//            URLImage urlImg = URLImage.createToStorage(enc, ar.getDesignation() + ar.getIdArticle(), url);
//
//            addButton(urlImg, ar.getDesignation(), true, ar.getNbReview(), 32);
//        }
        
        //display articles in panier 

        for (Article name : panierMap.keySet()) {
            Integer key = name.getIdArticle();
            Integer value = panierMap.get(name);
            System.out.println(key + " " + value);

            //ADDIN HERE
            url = "http://localhost/open-quantum-web/web/Uploads/Images/" + name.getImage();
            URLImage urlImg = URLImage.createToStorage(enc, name.getDesignation() + name.getIdArticle(), url);
            prixTotal += name.getPrixVente() * value;
            addButton(urlImg, name.getDesignation(), true, value, name.getPrixVente() * value, name);

        }
        Label totalLb = new Label("Total Price : " + prixTotal);
        Container cp = new Container(BoxLayout.y());
        Button aplyBtn = new Button("Apply");
        TextField promotxt = new TextField("", "enter code here");
        cp.addAll(promotxt, aplyBtn);
        Button valBtn = new Button("Buy Products");
        add(cntAll);
        add(totalLb);
        add(cp);
        ArrayList<Promotion> listepromo = PromotionService.getInstance().getAllPromotions();
        aplyBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                String codep = promotxt.getText().toString();
                System.out.println("" + codep);
                for (Promotion p : listepromo) {
                    if (p.getCode().equals(codep)) {
                        System.out.println("CODE S7I7");
                        int pourc = p.getPoucentage();
                        System.out.println("el pourcentage mtaa3 el code : " + pourc);

                        totaldis = prixTotal - ((prixTotal * pourc) / 100);
                        System.out.println("hesba jdida :" + totaldis);
                        totalLb.setText("Total After Discount : " + totaldis);
                    } else {
                    }
                }
            }
        });

        add(valBtn);
        LigneCommandeService lcs = LigneCommandeService.getInstance();
        
        //Buying articles 

        valBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {
                //SERVICE CALL

                if (panierMap.isEmpty()) {
                    Dialog.show("Produit", "Votre panier est vide ! Ajouter des article pour continuer l achat!", "OK", null);

                } else {
                    System.out.println("BUYING ARTICLES ");
                    boolean result = lcs.ConfirmAchat();
                    if (result) {
                       

                        System.out.println("SUCCESS");
                        panierMap.clear();
                        new PanierForm(PanierTheme, ListArticleForm.panierArticleStatic).show();

                    } else {
                        System.out.println("ERROR");
                    }

                }
            }
        });
    }
    
    

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();

    }

    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
//        if(img.getHeight() < size) {
//            img = img.scaledHeight(size);
//        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
//        }
//        ScaleImageLabel image = new ScaleImageLabel(img);
//        image.setUIID("Container");
//        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
        ScaleImageLabel imgg = new ScaleImageLabel();
        try {
            imgg = new ScaleImageLabel(GifImage.decode(getResourceAsStream("/paniergif.gif"), 5156565));
        } catch (IOException err) {
            log(err);
        }
        imgg.setUIID("Container");
        imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);

        Container page1
                = LayeredLayout.encloseIn(
                        imgg,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY( //                            new SpanLabel(text, "LargeWhiteText"),
                                //                            FlowLayout.encloseIn(likes, comments),
                                //                            spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }

    private void addButton(Image img, String title, boolean liked, int likeCount, int commentCount, Article art) {
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);

        Label remove = new Label("Remove");

        Button image = new Button(img.fill(width, height));
        image.setUIID("Label");

        Container cnt = BorderLayout.west(image);
        cnt.setLeadComponent(image);
        TextArea ta = new TextArea(title);
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(likeCount + " Qtn  ", "NewsBottomLine");
        likes.setTextPosition(RIGHT);
        if (!liked) {
            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
        } else {
            Style s = new Style(likes.getUnselectedStyle());
            s.setFgColor(0xff2d55);
            FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
            likes.setIcon(heartImage);
        }
        Label comments = new Label(commentCount + " DT", "NewsBottomLine");
        FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);

        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        ta,
                        BoxLayout.encloseX(likes, comments),
                        remove
                ));

        cntAll.add(cnt);
//       add(cnt);
//       cnt.removeAll();

        //delete produit
        image.addActionListener(e -> {
            if (Dialog.show("Confirm", "do you want to delete item?", "OK", "CANCEL")) {
                panierMap.remove(art);
                System.out.println("deleted");
                new PanierForm(PanierTheme, ListArticleForm.panierArticleStatic).show();
            }

        });

    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }

//    private HashMap<Article, Integer> formattedPanier(ArrayList<Article> currentPanier) {
//        HashMap<Article, Integer> formattedArticles = new HashMap<Article, Integer>();
//        int idArticle = 0;
//        int qtnArticle = 0;
//        for (Article ar1 : currentPanier) {
//            idArticle = ar1.getIdArticle();
//            if (formattedArticles.containsKey(ar1.getIdArticle()) == false) {
//                for (Article ar2 : currentPanier) {
//                    if (ar1.getIdArticle() == ar2.getIdArticle()) {
//                        qtnArticle++;
//                    }
//
//                }
//            formattedArticles.put(ar1, qtnArticle);
//
//                                      qtnArticle=0;
//
//            }
//
//        }
//
//        return formattedArticles;
//    }
}
