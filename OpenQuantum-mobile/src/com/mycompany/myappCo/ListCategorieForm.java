/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 /*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */
package com.mycompany.myappCo;

import Entites.Categorie;
import Entites.Famille;
import Entites.FluxMsg;
import Entites.Utilisateur;
import Services.ArticleService;
import Services.CategorieService;
import Services.ChatService;
import Services.FamilleService;
import Services.FilterService;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.components.ImageViewer;
import com.codename1.components.MultiButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.gif.GifImage;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.CENTER;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.FontImage;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.geom.Dimension;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.list.GenericListCellRenderer;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.plaf.UIManager;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class ListCategorieForm extends BaseForm {

    Resources reserveRes;
    int selectedUserId = 0;

    public ListCategorieForm(Resources res, Famille fam) {

        super("", BoxLayout.y());

        reserveRes = res;
        Toolbar tb = new Toolbar(true);

        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("");
        getContentPane().setScrollVisible(false);
        getContentPane().setScrollableX(false);
        getContentPane().setScrollableY(false);
        getContentPane().getAllStyles().setBorder(Border.createEmpty());
        getContentPane().getAllStyles().setBackgroundType(Style.BACKGROUND_NONE);
        getContentPane().getAllStyles().setBgTransparency(255);
        getContentPane().getAllStyles().setBgColor(0xeaeded);

        super.addSideMenu(res);

        tb.addSearchCommand(e -> {
        });

        tb.getAllStyles().setBorder(Border.createEmpty());
        tb.getAllStyles().setBackgroundType(Style.BACKGROUND_NONE);
//        tb.getAllStyles().setBgTransparency(255);
//        tb.getAllStyles().setBgColor(0xeaeded);
        Tabs swipe = new Tabs();
        Label spacer1 = new Label();
        Label spacer2 = new Label();
        Container c = new Container(new FlowLayout(CENTER));
        c.setUIID("Container");
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        Component.setSameSize(radioContainer, spacer1, spacer2);
//      add(LayeredLayout.encloseIn(swipe));
        Container Stylee = new Container(BoxLayout.y());
        Label test = new Label("hello");
        Label test2 = new Label("hello");
        Label test3 = new Label("hello");

        test2.getAllStyles().setFgColor(0xFFFFFF);
        test.getAllStyles().setFgColor(0xFFFFFF);
        test3.getAllStyles().setFgColor(0xFFFFFF);

        Stylee.add(test);
        Stylee.add(test2);
        Stylee.add(test3);

        Stylee.setScrollableY(false);

        ArticleService as = ArticleService.getInstance();
        FilterService fs = FilterService.getInstance();
   
        Container LF = new Container(new GridLayout(8, 1));
        LF.setScrollableY(true);

        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());

        Image img = res.getImage("Header-collection-M.jpg");

        if (img.getHeight() < size) {

            img = img.scaledHeight(size);

        }

        img = img.scaledHeight(550);

        ImageViewer ATT = new ImageViewer(img);
        add(ATT);
        CategorieService cs = CategorieService.getInstance();
        for (Categorie cat : cs.getCategorie()) {

            if (cat.getIdFamille().equals(fam)) {
                Container LC = new Container(BoxLayout.y());

                LC.getAllStyles().setMargin(Component.LEFT, 25);
                LC.getAllStyles().setMargin(Component.RIGHT, 25);
                LC.getAllStyles().setBorder(Border.createBevelLowered());
                LC.getAllStyles().setBackgroundType(Style.BACKGROUND_NONE);
                LC.getAllStyles().setBgTransparency(255);
                LC.getAllStyles().setBgColor(0xffffff);
                Container midC = FlowLayout.encloseCenterMiddle();
                Label lib = new Label(cat.getLibelle());
                lib.setUIID("Label3");
                midC.add(lib);

                EncodedImage enc = EncodedImage.createFromImage(res.getImage("loading.jpg"), true);
               ArrayList<String> listImg = fs.getImagesArticles(cat);

                   Container imagesCont = new Container( new GridLayout(1, listImg.size()));
                  System.out.println(listImg.size());


                for (int i = 0; i < listImg.size(); i++) {
                    String url;

                    String s = listImg.get(i);
                    url = Statics.IMG_URL+"/open-quantum-web/web/Uploads/Images/" + s;
                    System.out.println(url);
                    URLImage urlImg = URLImage.createToStorage(enc, s, url);
                    ImageViewer imggg = new ImageViewer(urlImg);
                    imagesCont.add(imggg);


                }

                LC.add(midC);
                LC.add(imagesCont);
                Container midC2 = FlowLayout.encloseCenterMiddle();
                Button btn2 = new Button("Afficher");
                btn2.addPointerPressedListener(l -> {
                    LC.getAllStyles().setBgColor(0xc0e7f8);
                   
                });

                btn2.addPointerReleasedListener(l -> {
                    LC.getAllStyles().setBgColor(0xffffff);
//                    System.out.println("actionHere");

                            ListArticleForm.setDisplayArticles(cat);
                            
                           new ListArticleForm(res,cat).show();
                    
                });

                btn2.addLongPressListener(l -> {
                    LC.getAllStyles().setBgColor(0xffffff);
                   
                });

                LC.setLeadComponent(btn2);

                add(LC);

                add(super.createLineSeparator(0xeaeded));
            }

        };


//        Container MidCont = FlowLayout.encloseCenterMiddle();
//        Label l = new Label("List Categories");
//        l.setUIID("Label2");
    }

}
