/*
 * Copyright (c) 2016, Codename One
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated 
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation 
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, 
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions 
 * of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF 
 * CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 * OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mycompany.myappCo;

import Entites.Utilisateur;
import Services.ArticleService;
import Services.AuthentificationSerivce;
import Services.FavorisService;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.components.FloatingHint;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ToastBar;
import com.codename1.gif.GifImage;
import com.codename1.io.Log;
import com.codename1.media.Media;
import com.codename1.media.MediaManager;
import com.codename1.ui.Button;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Label;
import com.codename1.ui.TextField;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.Resources;
import com.codename1.ui.util.UITimer;
import com.codename1.ui.validation.Constraint;
import com.codename1.ui.validation.Validator;
import java.io.IOException;

/**
 * Sign in UI
 *
 * @author Shai Almog
 */
public class SignInForm extends BaseForm {
 boolean res=false;
 
    public SignInForm(Resources res) {
        super(new BorderLayout());
        
        if(!Display.getInstance().isTablet()) {
            BorderLayout bl = (BorderLayout)getLayout();
            bl.defineLandscapeSwap(BorderLayout.NORTH, BorderLayout.EAST);
            bl.defineLandscapeSwap(BorderLayout.SOUTH, BorderLayout.CENTER);
        }
        getTitleArea().setUIID("Container");
        setUIID("SignIn");
        
        add(BorderLayout.NORTH, new Label(res.getImage("logooq.png"), "LogoLabel"));
        
        TextField username = new TextField("", "Username", 20, TextField.ANY);
        TextField password = new TextField("", "Password", 20, TextField.PASSWORD);
        username.setSingleLineTextArea(false);
        password.setSingleLineTextArea(false);
        Button signIn = new Button("Sign In");
        Button signUp = new Button("Sign Up");
        
        signUp.addActionListener(e ->{
              new ListSocieteForm(res).show();});
            
        signUp.setUIID("Link");
        Label doneHaveAnAccount = new Label("Don't have an account?");
        Container content = BoxLayout.encloseY(
                new FloatingHint(username),
                createLineSeparator(),
                new FloatingHint(password),
                createLineSeparator(),
                signIn,
                FlowLayout.encloseCenter(doneHaveAnAccount, signUp)
        );
        content.setScrollableY(true);
        add(BorderLayout.SOUTH, content);
        signIn.requestFocus();
        
       //
                ScaleImageLabel imgg = new ScaleImageLabel();

        try {
            imgg = new ScaleImageLabel(GifImage.decode(getResourceAsStream("/loading.gif"), 5156565));
        } catch (IOException err) {
            log(err);
        }
        
         try {
        Media m = MediaManager.createMedia((Display.getInstance().getResourceAsStream(getClass(), "/startup.mp3")), "audio/mpeg");
        m.play();
    } catch (IOException err) {
        Log.e(err);
    }
        
        Label animationGif = new Label("imgg");
        imgg.getAllStyles().setBgTransparency(0);

        Dialog loadingAnimation = new Dialog();
        loadingAnimation.setLayout(new BorderLayout(BorderLayout.CENTER_BEHAVIOR_CENTER_ABSOLUTE));
        Style dlgStyle = loadingAnimation.getDialogStyle();
        dlgStyle.setBorder(Border.createEmpty());
        dlgStyle.setBgTransparency(0);
        loadingAnimation.add(BorderLayout.CENTER, imgg);

        //
        signIn.addActionListener(e -> {
            AuthentificationSerivce as = AuthentificationSerivce.getInstance();
            //LOADING HERE
            Log.p("CONSUMING WEB SERVICE // TASK IS RUNNING // SWTICHING TO THE OTHER FORM WHEN DONE");
            // Remove the UITimer and add your network tasks
            // Call loadingAnimation.dispose() when the tasks are ended

            if (!checkCredentials(username, password)) {
                            loadingAnimation.showModeless();

                if (as.checkLogin(username.getText(), password.getText())) {
                    CurrentSession.loggedIn = true;
                    Utilisateur currentLoggedUser = as.getUser(username.getText(), password.getText()).get(0);
                    Statics.loggedUser = currentLoggedUser;
                    //  CurrentSession.user_id_societe=as.getUser(username.getText(),password.getText()).get(TOP);
                    ArticleService Sa = ArticleService.getInstance();
                    FavorisService fs = FavorisService.getInstance();
                    new UITimer(() -> {
                      Sa.fetchArticles();
                        System.out.println(fs.getArticleFavoris());
                      
                          loadingAnimation.dispose();
                        new ListFamilleForm(res).show();
                    }).schedule(0, false, loadingAnimation);
                } else {

                    ToastBar.showMessage("username or password is incorrect ", FontImage.MATERIAL_INFO);

                }
            }
                        

            //
        });
        
    }
    
    public boolean checkCredentials(TextField username,TextField password){
           
        Validator validator = new Validator();
         validator.setShowErrorMessageForFocusedComponent(true);
          validator.addConstraint(username, new Constraint() {
            @Override
            public boolean isValid(Object value) {
              //  boolean res = false;
                if (value.equals("")) {
                     username.setUIID("InvalidTextField");
                     System.out.println("invalid text field");
                    res = true;
                   
                   
                }else{
                    res = false;
                }
                return res;
            }

            @Override
            public String getDefaultFailMessage() {
                return "Please Enter a valid username";
            }

        });
          
          
            validator.addConstraint(password, new Constraint() {
            @Override
            public boolean isValid(Object value) {
              // boolean res = false;
                if (value.equals("")) {
                      password.setUIID("InvalidTextField");
                     System.out.println("invalid text field");
                    res = true;
                }else{
                    res = false;
                }
                return res;
            }

            @Override
            public String getDefaultFailMessage() {
                return "Please Enter a valid password";
            }

        });
        System.out.println(res);
         return res;
    }
    
}
