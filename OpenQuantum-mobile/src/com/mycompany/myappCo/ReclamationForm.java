/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Livraison;
import Entites.reclamation;
import Services.ReclamationService;
import com.codename1.components.FloatingActionButton;
import com.codename1.components.MultiButton;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ToastBar;
import com.codename1.gif.GifImage;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import static com.codename1.ui.CN.getResourceAsStream;
import static com.codename1.ui.CN.log;
import com.codename1.ui.ComboBox;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Display;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Label;
import com.codename1.ui.Tabs;
import com.codename1.ui.Toolbar;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.util.Resources;
import com.codename1.ui.Image;
import com.codename1.ui.RadioButton;
import com.codename1.ui.TextArea;
import com.codename1.ui.TextField;
import com.codename1.ui.events.ActionEvent;
import com.codename1.ui.events.ActionListener;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.sun.java.accessibility.util.SwingEventMonitor;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class ReclamationForm extends BaseForm {
    Resources reserveRes;
     Container cntAll = new Container(BoxLayout.y());
    public ReclamationForm(Resources res) {

        super("reclamation",BoxLayout.y());
           reserveRes=res;
        Toolbar tb = new Toolbar(true);
        
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle("Reclamation");
        getContentPane().setScrollVisible(false);
        
        super.addSideMenu(res);
        tb.addSearchCommand(e -> {});
        
        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        addTab(swipe, res.getImage("delivery.png"), spacer1, " ", " ", "All Deliveries ");
                
        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();
        
         ButtonGroup bg = new ButtonGroup();
        int size = Display.getInstance().convertToPixels(1);
        Image unselectedWalkthru = Image.createImage(size, size, 0);
        Graphics g = unselectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAlpha(100);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        Image selectedWalkthru = Image.createImage(size, size, 0);
        g = selectedWalkthru.getGraphics();
        g.setColor(0xffffff);
        g.setAntiAliased(true);
        g.fillArc(0, 0, size, size, 0, 360);
        RadioButton[] rbs = new RadioButton[swipe.getTabCount()];
        FlowLayout flow = new FlowLayout(CENTER);
        flow.setValign(BOTTOM);
        Container radioContainer = new Container(flow);
        for (int iter = 0; iter < rbs.length; iter++) {
            rbs[iter] = RadioButton.createToggle(unselectedWalkthru, bg);
            rbs[iter].setPressedIcon(selectedWalkthru);
            rbs[iter].setUIID("Label");
            radioContainer.add(rbs[iter]);
        }
         rbs[0].setSelected(true);
        swipe.addSelectionListener((i, ii) -> {
            if(!rbs[ii].isSelected()) {
                rbs[ii].setSelected(true);
            }
        });
        
        Component.setSameSize(radioContainer, spacer1, spacer2);
        add(LayeredLayout.encloseIn(swipe, radioContainer));
        
        ButtonGroup barGroup = new ButtonGroup();
        RadioButton all = RadioButton.createToggle("All", barGroup);
        all.setUIID("SelectBar2");
        RadioButton featured = RadioButton.createToggle("livraison", barGroup);
        featured.setUIID("SelectBar2");
        RadioButton popular = RadioButton.createToggle("article", barGroup);
        popular.setUIID("SelectBar2");
        RadioButton myFavorite = RadioButton.createToggle("technique", barGroup);
        myFavorite.setUIID("SelectBar2");
        Label arrow = new Label(res.getImage("news-tab-down-arrow.png"), "Container");
        
        add(LayeredLayout.encloseIn(
                GridLayout.encloseIn(4, all, featured, popular, myFavorite),
                FlowLayout.encloseBottom(arrow)
        ));
       
        
        Container round = new Container();
        FloatingActionButton fab = FloatingActionButton.createFAB(FontImage.MATERIAL_ADD);
        FloatingActionButton mg = fab.createSubFAB(FontImage.MATERIAL_PLACE, "livraison");
        FloatingActionButton ab = fab.createSubFAB(FontImage.MATERIAL_IMPORT_CONTACTS, "article");
        
         FlowLayout flows = new FlowLayout(CENTER);
        flows.setValign(CENTER);
        round.setLayout(flows);
        round.add(fab);
        super.add(round);
        //reclamation rec = Reclamation;
        mg.addActionListener(l -> {
           System.out.println("Swapping to Livraison reclamation");
         
          ToastBar.Status status = ToastBar.getInstance().createStatus();
              status.setMessage("Swapping to Livraison reclamation");
              status.setShowProgressIndicator(true);
              status.show();
            new ReclamationLivraisonForm(reserveRes).show();
//
//        reserveRes = res;
//        Toolbar tbs = new Toolbar(true);
//
//        setToolbar(tbs);
//        getTitleArea().setUIID("Container");
//        setTitle("OpenQuantum");
//        getContentPane().setScrollVisible(false);
////            getContentPane().setScrollableX(false);
////                   getContentPane().setScrollableY(false);
//
//        super.addSideMenu(res);
//
//        tb.addSearchCommand(l -> {
//        });
//
//        tb.getAllStyles().setBorder(Border.createEmpty());
//        tb.getAllStyles().setBackgroundType(Style.BACKGROUND_NONE);
//        tb.getAllStyles().setBgTransparency(255);
//        tb.getAllStyles().setBgColor(0x99CCCC);
//        Tabs swipes = new Tabs();
//        Label spacer10 = new Label();
//        Label spacer20 = new Label();
//        Container c = new Container(new FlowLayout(CENTER));
//        c.setUIID("Container");
//        FlowLayout flows1 = new FlowLayout(CENTER);
//        flows1.setValign(BOTTOM);
//        Container radioContainers = new Container(flows1);
//        Component.setSameSize(radioContainers, spacer10, spacer20);
//        
//            Container MidCont = FlowLayout.encloseCenterMiddle();
//        Label lb = new Label("livraison");
//        Container cnt = new Container(BoxLayout.x());
//        Label lbm = new Label("Modele");
//        TextField tfm = new TextField();
//        tfm.setHint("description");
//        MidCont.add(lb);
//        cnt.addAll(lbm,tfm);
//        super.addAll(MidCont,cnt);
//        super.show();
//        if(mg.isSelected())
//        super.addAll(MidCont,cnt);
//        
//        
//        //if(cb.getSelectedIndex()==1)
//           // note.addAll(lbm,tfm);
//                 
//        
          
        });
         ab.addActionListener(r -> {
//             ToastBar.Status status = ToastBar.getInstance().createStatus();
//              status.setMessage("Swapping to Article reclamation");
//              status.setShowProgressIndicator(true);
//              status.show();
              
              new ReclamationArticleForm(reserveRes).show();
         });
        //Container ReclamationList = new Container(BoxLayout.y());
        ReclamationService rec1 = ReclamationService.getInstance();
        for (reclamation ar : rec1.getAllReclamation()){
//        for (int i = 0; i < rec1.size(); i++) {
//            int id = rec1.get(i).getIdrec();
//            String nature = rec.get(i).getNaturerec();
//            String desc = Reclamations.get(i).getDescription();
//            String etat = rec.get(i).getStat();
//            String livr = rec.get(i).getIdLivraison().getTrackingCode();
//            String nonart = rec.get(i).getIdarticle().getDesignation();
            addButton(ar.getNaturerec(), ar.getDescription(), false, ar.getStat(), ar.getIdLivraison().getTrackingCode(), ar.getIdarticle().getDesignation(), ar);
        }
        add(cntAll);
     super.show();
    }     
private void addButton(String naturerec, String description, boolean liked, String stat, String trackingCode, String designation, reclamation rec){
  
     
    reclamation recl = rec;
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);
        
        Label ellihowa = new Label(naturerec);
       Container cnt = BorderLayout.west(ellihowa);
        TextArea ta = new TextArea();
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(description + "", "NewsBottomLine");
        likes.setTextPosition(RIGHT);
        if (!liked) {
            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
        } else {
            Style s = new Style(likes.getUnselectedStyle());
            s.setFgColor(0xff2d55);
            FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
            likes.setIcon(heartImage);
        }
        Label comments = new Label(stat , "NewsBottomLine");
        FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);
        
        Button btnDelete=new Button("Delete");
        btnDelete.addActionListener(l->{
         ReclamationService rec1 = ReclamationService.getInstance();
         rec1.delete(rec.getIdrec());
         cntAll.removeComponent(cnt);
    
                cntAll.animateLayout(150);
            System.out.println("worrrks");
        
        });
        
        
        
        

        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        ta,
                        BoxLayout.encloseX(likes, comments)
                ));
        
          cnt.add(BorderLayout.EAST,
               btnDelete);
               
       cntAll.add(cnt);

}
    private void addTab(Tabs swipe, Image image, Label spacer1, String likesStr, String commentsStr, String all_Deliveries_) {
           int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
         
//        if(img.getHeight() < size) {
//            img = img.scaledHeight(size);
//        }
        Label likes = new Label(likesStr);
        Style heartStyle = new Style(likes.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likes.setIcon(heartImage);
        likes.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
//        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
//            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
//        }
//        ScaleImageLabel image = new ScaleImageLabel(img);
//        image.setUIID("Container");
//        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");
         ScaleImageLabel   imgg=new ScaleImageLabel() ;
                try {
      imgg= new ScaleImageLabel(GifImage.decode(getResourceAsStream("/OpSQ.gif"), 5156565));
} catch(IOException err) {
    log(err);
}
                imgg.setUIID("Container");
                imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
//                imgg.getAllStyles().setBorder(Border.createEmpty());
//                imgg.getAllStyles().setBgTransparency(255);
        Container page1 = 
            LayeredLayout.encloseIn(
                imgg,
                overlay,
                BorderLayout.south(
                    BoxLayout.encloseY(
//                            new SpanLabel(text, "LargeWhiteText"),
//                            FlowLayout.encloseIn(likes, comments),
//                            spacer
                        )
                )
            );

        swipe.addTab("", page1);
    }
//    private void addTab1(Tabs swipes, Image image, Label spacer10, String likesStr, String commentsStr, String all_Deliveries_) {
//           int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
//         
////        if(img.getHeight() < size) {
////            img = img.scaledHeight(size);
////        }
//        Label likes = new Label(likesStr);
//        Style heartStyle = new Style(likes.getUnselectedStyle());
//        heartStyle.setFgColor(0xff2d55);
//        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
//        likes.setIcon(heartImage);
//        likes.setTextPosition(RIGHT);
//
//        Label comments = new Label(commentsStr);
//        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
////        if(img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
////            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
////        }
////        ScaleImageLabel image = new ScaleImageLabel(img);
////        image.setUIID("Container");
////        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
//        Label overlay = new Label(" ", "ImageOverlay");
//         ScaleImageLabel   imgg=new ScaleImageLabel() ;
//                try {
//      imgg= new ScaleImageLabel(GifImage.decode(getResourceAsStream("/rec.gif"), 5156565));
//} catch(IOException err) {
//    log(err);
//}
//                imgg.setUIID("Container");
//                imgg.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
////                imgg.getAllStyles().setBorder(Border.createEmpty());
////                imgg.getAllStyles().setBgTransparency(255);
//        Container page1 = 
//            LayeredLayout.encloseIn(
//                imgg,
//                overlay,
//                BorderLayout.south(
//                    BoxLayout.encloseY(
////                            new SpanLabel(text, "LargeWhiteText"),
////                            FlowLayout.encloseIn(likes, comments),
////                            spacer
//                        )
//                )
//            );
//
//        swipes.addTab("", page1);
//    }
}
