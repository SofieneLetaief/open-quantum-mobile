/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myappCo;

import Entites.Article;
import Services.FavorisService;
import Utils.Statics;
import com.codename1.components.ImageViewer;
import com.codename1.components.ScaleImageLabel;
import com.codename1.components.ShareButton;
import com.codename1.components.SpanLabel;
import com.codename1.components.ToastBar;
import com.codename1.io.FileSystemStorage;
import com.codename1.io.Log;
import com.codename1.notifications.LocalNotification;
import com.codename1.ui.Button;
import com.codename1.ui.ButtonGroup;
import com.codename1.ui.Component;
import static com.codename1.ui.Component.BOTTOM;
import static com.codename1.ui.Component.CENTER;
import static com.codename1.ui.Component.LEFT;
import static com.codename1.ui.Component.RIGHT;
import com.codename1.ui.Container;
import com.codename1.ui.Dialog;
import com.codename1.ui.Display;
import com.codename1.ui.EncodedImage;
import com.codename1.ui.Font;
import com.codename1.ui.FontImage;
import com.codename1.ui.Form;
import com.codename1.ui.Graphics;
import com.codename1.ui.Image;
import com.codename1.ui.Label;
import com.codename1.ui.RadioButton;
import com.codename1.ui.Tabs;
import com.codename1.ui.TextArea;
import com.codename1.ui.Toolbar;
import com.codename1.ui.URLImage;
import com.codename1.ui.layouts.BorderLayout;
import com.codename1.ui.layouts.BoxLayout;
import com.codename1.ui.layouts.FlowLayout;
import com.codename1.ui.layouts.GridLayout;
import com.codename1.ui.layouts.LayeredLayout;
import com.codename1.ui.plaf.Border;
import com.codename1.ui.plaf.Style;
import com.codename1.ui.util.ImageIO;
import com.codename1.ui.util.Resources;
import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author Letaief Sofiene
 */
public class DetailsArticleForm extends BaseForm {

    Resources reserveRes;
    Label likesTest = new Label();

    public DetailsArticleForm(Resources res, Article art) {
        super("", BoxLayout.y());
        reserveRes = res;
        Toolbar tb = new Toolbar(true);
        setToolbar(tb);
        getTitleArea().setUIID("Container");
        setTitle(art.getDesignation());
        Form previous = Display.getInstance().getCurrent();
        getContentPane().setScrollVisible(false);

        tb.setBackCommand("", e -> {
            previous.showBack();
        });

        Tabs swipe = new Tabs();

        Label spacer1 = new Label();
        Label spacer2 = new Label();
        EncodedImage enc = EncodedImage.createFromImage(res.getImage("loading.jpg"), true);
        String url = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + art.getImage();
        URLImage urlImg = URLImage.createToStorage(enc, art.getImage(), url);
        addTab(swipe, urlImg, spacer1, art.getNbLike() + " Likes  ", art.getNbReview() + " Comments", art.getDesignation());
        // addTab(swipe, res.getImage("dog.jpg"), spacer2, "100 Likes  ", "66 Comments", "Dogs are cute: story at 11");

        swipe.setUIID("Container");
        swipe.getContentPane().setUIID("Container");
        swipe.hideTabs();
        add(LayeredLayout.encloseIn(swipe));

        Container deb = new Container(BoxLayout.y());
        Container cnt2 = new Container(BoxLayout.x());
        Container cntstock = new Container(BoxLayout.x());

        if (art.getQteStock() != 0) {
            int height = Display.getInstance().convertToPixels(11.5f);
            int width = Display.getInstance().convertToPixels(14f);
            Label labStock = new Label("En stock:");
            Label piece = new Label("Encore " + art.getQteStock() + " piece disponible ");
            labStock.setUIID("GreenLabel");
            cntstock.add(labStock);

            cntstock.add(piece);

            Button plus = new Button(res.getImage("plus.png"));
            Button minus = new Button(res.getImage("minus.png"));
            plus.setUIID("Label");
            minus.setUIID("Label");
            Component.setSameSize(plus, minus);
            Button btnAjout = new Button("Add To Cart");

            Label count = new Label("1");

            cnt2.addAll(minus, count, plus, btnAjout);

            plus.addActionListener(e -> {
//                if(Integer.parseInt(count.getText())>=art.getQteStock()){
//                    
//                } //ne9es les verif apres    

                int x = Integer.parseInt(count.getText());
                if (x >= art.getQteStock()) {
                    Dialog.show("Out Of Stock", "Quantités existant:" + art.getQteStock(), "OK", null);
                } else {
                    x++;
                    count.setText(String.valueOf(x));
                }

            });

            minus.addActionListener(e -> {
                int x = Integer.parseInt(count.getText());

                if (x <= 1) {
                    Dialog.show("ERROR", "Quantité Minimale  1 !", "OK", null);
                } else {
                    x--;
                    count.setText(String.valueOf(x));
                }

            });

            btnAjout.addActionListener(e -> {
                if (PanierForm.panierMap.containsKey(art)) {
                    int newQtn = 0;
                    System.out.println("ARTICLE EXIST");
                    int oldQtn = PanierForm.panierMap.get(art);
                    System.out.println("Old qtn " + oldQtn);
                    newQtn = (oldQtn + Integer.parseInt(count.getText().toString()));
                    System.out.println("new QTn " + newQtn);
                    PanierForm.panierMap.put(art, oldQtn + Integer.parseInt(count.getText().toString()));

                } else {
                    System.out.println("" + art.getIdArticle());
                    int qtn = Integer.parseInt(count.getText().toString());
                    System.out.println("qtn " + qtn);
                    PanierForm.panierMap.put(art, Integer.parseInt(count.getText().toString()));
                    System.out.println("MAP SIZE " + PanierForm.panierMap.size());
                }

                Dialog.show("Produit", "Produit ajouté avec succes", "OK", null);

            });

        } else {         
            Label outofStock = new Label("Out of Stock");
            outofStock.setUIID("RedLabel");
            cntstock.add(outofStock);

        }
        Container Desc = new Container(BoxLayout.x());
        Label description = new Label("Descirption : ");
        description.setUIID(url);
        Desc.add(description);
        Desc.add(new SpanLabel(art.getDescirption()));
        Desc.getAllStyles().setBorder(Border.createBevelLowered());
        deb.add(cntstock);
        deb.add(Desc);
        Container cnt = BorderLayout.west(new Label(""));
        add(deb);
        
//        Image screenshot = Image.createImage(this.getWidth(), this.getHeight());
    
           URLImage urlImgQrCode = URLImage.createToStorage(enc,"qrCode"+art.getIdArticle()+".png","https://chart.googleapis.com/chart?cht=qr&choe=UTF-8&chs=100x100&chl="+art.getIdArticle())  ;
           ScaleImageLabel imageTest = new ScaleImageLabel(urlImgQrCode);
      
           Container FakeImg =new Container(BoxLayout.y());
       
           FakeImg.add(imageTest);
         
           
//         try(OutputStream os = FileSystemStorage.getInstance().openOutputStream(imageFile)) {
//             ImageIO.getImageIO().save(screenshot, os, ImageIO.FORMAT_PNG, 1);
//                } catch(IOException err) {
//                 Log.e(err);
//                }
         
     
        ShareButton sb = new ShareButton();
        
        sb.setImageToShare(FileSystemStorage.getInstance().getAppHomePath()+"QrCode"+art.getImage(), "image/png");
        
       // sb.setImageToShare("https://chart.googleapis.com/chart?cht=qr&choe=UTF-8&chs=100x100&chl=sofiennee", "https://chart.googleapis.com/chart?cht=qr&choe=UTF-8&chs=100x100&chl=sofiennee");
        sb.setTextToShare(Statics.BASE_URL + "/ListArticle/" + art.getCategorie().getLibelle());

       // String imageFile = FileSystemStorage.getInstance().getAppHomePath();
        
        Font fnt = Font.createTrueTypeFont("fontello", "fontello.ttf");
        int size = Display.getInstance().convertToPixels(5);
        FontImage emptyHeart = FontImage.createFixed("\uE800", fnt, 0xff2d55, size, size);
        FontImage fullHeart = FontImage.createFixed("\uE801", fnt, 0xff2d55, size, size);
        Button btnHeart = new Button();

        if (art.isFavoris() == true) {
            btnHeart.setIcon(fullHeart);
        } else {
            btnHeart.setIcon(emptyHeart);
        }

        int x = 0;

        FavorisService fs = FavorisService.getInstance();

        btnHeart.setUIID("Label");

        btnHeart.addActionListener(tt -> {
            if (btnHeart.getIcon() == emptyHeart) {
                btnHeart.setIcon(fullHeart);
                ToastBar.showMessage(art.getDesignation() + " Added to Favorite ", FontImage.MATERIAL_FAVORITE, 4000);
                likesTest.setText((art.getNbLike() + 1) + " Likes");
                art.setFavoris(true);
                art.setNbLike(art.getNbLike() + 1);
                fs.addArticleFavoris(art.getIdArticle());
                fs.ajoutArticleToList(art);
                LocalNotification n = new LocalNotification();
                n.setAlertBody("Avez Vous Oublié votre "+art.getDesignation()+" ? Cliquez ici pour l'avoir");
                n.setAlertTitle("Hello");
                n.setId( Integer.toString(art.getIdArticle()));
                Display.getInstance().scheduleLocalNotification(n, System.currentTimeMillis() +  10 * 100, LocalNotification.REPEAT_NONE);

//          ToastBar.showConnectionProgress(url, cr, onSuccess, onError);
            } else {
                btnHeart.setIcon(emptyHeart);
                ToastBar.showMessage(art.getDesignation() + " Removed from Favorite ", FontImage.MATERIAL_FAVORITE, 4000);
                likesTest.setText((art.getNbLike() - 1) + " Likes");
                art.setNbLike(art.getNbLike() - 1);
                art.setFavoris(false);
                 fs.deleteArticleToList(art);
                fs.removeArticleFavoris(art.getIdArticle());
                 Display.getInstance().cancelLocalNotification(Integer.toString(art.getIdArticle()));

            }

        });

        cnt.add(BorderLayout.WEST,
                BoxLayout.encloseY(
                        BoxLayout.encloseX(cnt2)));

        cnt.add(BorderLayout.EAST, BoxLayout.encloseX(sb, btnHeart));

        add(cnt);
//        Component.setSameSize(radioContainer, spacer1, spacer2);

        int height2 = Display.getInstance().convertToPixels(25f);
        int width2 = Display.getInstance().convertToPixels(25f);
        Container c = new Container(BoxLayout.x());
        c.setScrollableX(true);
//        for (int i = 0; i < 10; i++) {
//            Button image = new Button(urlImg.fill(width2, height2));
//            image.setUIID("Label");
//            c.add(image);
//        }
        String url2;
        URLImage urlImg2;
        if (ListArticleForm.displayArticles.size() > 1) {

            for (Article ar : ListArticleForm.displayArticles) {
                if (ar.getImage() != art.getImage()) {
                    Container c2 = new Container(BoxLayout.y());

                    url2 = Statics.IMG_URL + "/open-quantum-web/web/Uploads/Images/" + ar.getImage();
                    urlImg2 = URLImage.createToStorage(enc, ar.getImage(), url);
                    Button image = new Button(urlImg2.fill(width2, height2));
                    image.setUIID("Label");
                    Button details = new Button("Details");
                    c2.add(image);
                    c2.add(new SpanLabel(ar.getDesignation()));
                    c2.add(new Label(String.valueOf(ar.getPrixVente()) + "$"));
                    c2.add(details);
                    c2.getAllStyles().setBorder(Border.createBevelLowered());
                    c2.getAllStyles().setBgTransparency(255);
                    details.addActionListener(l -> {
                        c2.getAllStyles().setBgColor(0xc0e7f8);

                    });

                    details.addLongPressListener(l -> {
                        c2.getAllStyles().setBgColor(0xffffff);

                    });

                    details.addPointerReleasedListener(l -> {
                        c2.getAllStyles().setBgColor(0xffffff);
                        new DetailsArticleForm(reserveRes, ar).show();
                    });

                    c.add(c2);

                }
            }
            Label addrev = new Label("Ajouter Review");
            addrev.addPointerPressedListener(aaa -> {
                new ReviewForm(reserveRes, art).show();
            });
            addrev.setUIID(("Label"));
            add(addrev);

            Container midC2 = FlowLayout.encloseCenterMiddle();
            Label prod = new Label("produit similaire");
            prod.setUIID(url);
            midC2.add(prod);
            add(midC2);
            add(c);
            add(FakeImg);
        }
    }

    private void updateArrowPosition(Button b, Label arrow) {
        arrow.getUnselectedStyle().setMargin(LEFT, b.getX() + b.getWidth() / 2 - arrow.getWidth() / 2);
        arrow.getParent().repaint();

    }
  //https://chart.googleapis.com/chart?cht=qr&chl=sofiennee&choe=UTF-8&chs=500x500
    
    private void addTab(Tabs swipe, Image img, Label spacer, String likesStr, String commentsStr, String text) {
        int size = Math.min(Display.getInstance().getDisplayWidth(), Display.getInstance().getDisplayHeight());
        if (img.getHeight() < size) {
            img = img.scaledHeight(size);
        }
        // Label likes = new Label(likesStr);

        this.likesTest.setText(likesStr);
        Style heartStyle = new Style(likesTest.getUnselectedStyle());
        heartStyle.setFgColor(0xff2d55);
        FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, heartStyle);
        likesTest.setIcon(heartImage);
        likesTest.setTextPosition(RIGHT);

        Label comments = new Label(commentsStr);
        FontImage.setMaterialIcon(comments, FontImage.MATERIAL_CHAT);
        if (img.getHeight() > Display.getInstance().getDisplayHeight() / 2) {
            img = img.scaledHeight(Display.getInstance().getDisplayHeight() / 2);
        }

        ScaleImageLabel image = new ScaleImageLabel(img);
        image.setUIID("Container");
        image.setBackgroundType(Style.BACKGROUND_IMAGE_SCALED_FILL);
        Label overlay = new Label(" ", "ImageOverlay");

        Container page1
                = LayeredLayout.encloseIn(
                        image,
                        overlay,
                        BorderLayout.south(
                                BoxLayout.encloseY(
                                        new SpanLabel(text, "LargeWhiteText"),
                                        FlowLayout.encloseIn(likesTest, comments),
                                        spacer
                                )
                        )
                );

        swipe.addTab("", page1);
    }

    private void addButton(Image img, String title, boolean liked, int likeCount, int commentCount) {
        int height = Display.getInstance().convertToPixels(11.5f);
        int width = Display.getInstance().convertToPixels(14f);
        Button image = new Button(img.fill(width, height));
        image.setUIID("Label");
        Container cnt = BorderLayout.west(image);
        cnt.setLeadComponent(image);
        TextArea ta = new TextArea(title);
        ta.setUIID("NewsTopLine");
        ta.setEditable(false);

        Label likes = new Label(likeCount + " Likes  ", "NewsBottomLine");
        likes.setTextPosition(RIGHT);
        if (!liked) {
            FontImage.setMaterialIcon(likes, FontImage.MATERIAL_FAVORITE);
        } else {
            Style s = new Style(likes.getUnselectedStyle());
            s.setFgColor(0xff2d55);
            FontImage heartImage = FontImage.createMaterial(FontImage.MATERIAL_FAVORITE, s);
            likes.setIcon(heartImage);
        }
        Label comments = new Label(commentCount + " Comments", "NewsBottomLine");
        FontImage.setMaterialIcon(likes, FontImage.MATERIAL_CHAT);

        cnt.add(BorderLayout.CENTER,
                BoxLayout.encloseY(
                        ta,
                        BoxLayout.encloseX(likes, comments)
                ));
        add(cnt);
        image.addActionListener(e -> ToastBar.showMessage(title, FontImage.MATERIAL_INFO));
    }

    private void bindButtonSelection(Button b, Label arrow) {
        b.addActionListener(e -> {
            if (b.isSelected()) {
                updateArrowPosition(b, arrow);
            }
        });
    }
}
