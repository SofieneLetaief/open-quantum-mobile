/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Livraison;
import Entites.Livreur;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import com.codename1.processing.Result;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author HP OMEN
 */
public class LivraisonService {

    public static LivraisonService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Livraison> livraisons, livreurs;

    public LivraisonService() {
        req = new ConnectionRequest();

    }

    public static LivraisonService getInstance() {
        if (instance == null) {
            instance = new LivraisonService();
        }
        return instance;
    }

    //Parsing user 
    public ArrayList<Livraison> parseLivraisons(String jsonText) {
        try {
            livraisons = new ArrayList<>();
            livreurs = new ArrayList<>();

            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");

            for (Map<String, Object> obj : list) {
                Livraison livraison = new Livraison();
                float id = Float.parseFloat(obj.get("Id").toString());
                livraison.setIdLivraison((int) id);
                if (obj.get("trackingcode") == null) {
                    livraison.setTrackingCode("NO TRACKING CODE");

                } else {
                    livraison.setTrackingCode(obj.get("trackingcode").toString());
                }

                livraison.setStatus(obj.get("Status").toString());

                //java.util.List<Livreur> livreur =  (java.util.List<Livreur>)obj.get("Livreur"); 
                //Result result = Result.fromContent(tasksListJson);
//                String para = result.getAsString("trackingcode");
//                    System.out.println("image name "+para);
                //JSONArray arr = (JSONArray) obj.get("Livreur");
                /////test
                //
                LinkedHashMap<String, Object> lhm = (LinkedHashMap<String, Object>) obj.get("Livreur");

                //List<String> listKeys = new ArrayList<String>(lhm.keySet());
//                System.out.println("List contains:");
//                for (String key : listKeys) {
//                    System.out.println(key);
//                }
                //work here
                Livreur livreur = new Livreur();
                livreur.setIdLivreur((double) lhm.get("idlivreur"));
                livreur.setAdresse((String) lhm.get("adresse"));
                livreur.setImage((String) lhm.get("image"));
                livreur.setNom((String) lhm.get("nom"));
                livreur.setPrenom((String) lhm.get("prenom"));
                livreur.setNumtel((double) lhm.get("numtel"));

                livraison.setLivreur(livreur);

                //
//                System.out.println(livreur.toString());
                //
////
//                for (Map.Entry<String, Object> entry : lhm.entrySet()) {
//                    String name = entry.getKey();
//                    //Do something
//                    //
//                   Livreur livreur = new Livreur();
//                    System.out.println(entry.getValue());
//
//                }
//
//                //
//                System.out.println("printing livreur here ");
//                System.out.println(obj.get("Livreur"));
//                Map<String, Object> date = (Map<String, Object>) obj.get("Datecreation");
//                Date dateCreation = (Date) date.get("date");
//
//                DateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
//                Date date = format.parse(string);
//
//                System.out.println(dateCreation);
                livraisons.add(livraison);
            }

//            for(Livraison l : livList){
//                System.out.println(l.toString());
//            }
            //testing threaad
            //
        } catch (IOException ex) {;

        }
        return livraisons;
    }

    //retrieving connected user
    public ArrayList<Livraison> getLivraisons(int idSociete) {
        String url = Statics.BASE_URL + "/listLivraison";
        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("idSociete", "" + idSociete);
        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                livraisons = parseLivraisons(new String(r.getResponseData()));
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return livraisons;

        ///
    }

}
