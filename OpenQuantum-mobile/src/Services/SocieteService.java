/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Utilisateur;
import Entites.Societe;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class SocieteService {

    public static SocieteService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Societe> Societes;

    public SocieteService() {
        req = new ConnectionRequest();

    }

    public static SocieteService getInstance() {
        if (instance == null) {
            instance = new SocieteService();
        }
        return instance;
    }


    public ArrayList<Societe> parseSocietes(String jsonText) {
        try {
            Societes = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");

            for (Map<String, Object> obj : list) {
                Societe Societe = new Societe();
                float id = Float.parseFloat(obj.get("idsociete").toString());
                Societe.setIdSociete((int)id);
                
                
                Societe.setRaisonSociale(obj.get("raisonsociale").toString());
                Societe.setMatFiscale(obj.get("matfiscale").toString());
                Societe.setNumTel1((int)Float.parseFloat(obj.get("numtel1").toString()));
                Societe.setNumTel2((int)Float.parseFloat(obj.get("numtel2").toString()));
                Societe.setEmail(obj.get("email").toString());
                Societe.setAdresse(obj.get("adresse").toString());
                Societe.setVille(obj.get("ville").toString());
                Societe.setCodePostal((int) Float.parseFloat(obj.get("codepostal").toString()));
                Societe.setPays(obj.get("pays").toString());
                Societe.setDevise(obj.get("devise").toString());
                Societe.setLogo(obj.get("logo").toString());

             
                Societes.add(Societe);
            }

        } catch (IOException ex) {;

        }
        return Societes;
    }

    public ArrayList<Societe> getAllSocietes(){
        String url = Statics.BASE_URL+"/listSociete";
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                Societes = parseSocietes(new String(req.getResponseData()));
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return Societes;
    }
}
