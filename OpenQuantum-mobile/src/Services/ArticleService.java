/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Utilisateur;
import Entites.Article;
import Entites.Categorie;
import Entites.Constructeur;
import Entites.Famille;
import Entites.Modele;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class ArticleService {

    public static ArticleService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Article> Articles;

    public ArticleService() {
        req = new ConnectionRequest();

    }

    public static ArticleService getInstance() {
        if (instance == null) {
            instance = new ArticleService();
        }
        return instance;
    }


    public ArrayList<Article> parseArticles(String jsonText) {
        try {
            CategorieService cs=CategorieService.getInstance();
            ConstructeurService conserv=ConstructeurService.getInstance();
            ModeleService ms=ModeleService.getInstance();
            FamilleService fams=FamilleService.getInstance();
            FilterService fs =FilterService.getInstance();
            
            Articles = new ArrayList<>(100);
                    LinkedHashSet<Categorie> ListCategories =   new LinkedHashSet<Categorie>();  
                    LinkedHashSet<Famille> ListFamilles =   new LinkedHashSet<Famille>();  
                    LinkedHashSet<Constructeur> ListConstructeurs =   new LinkedHashSet<Constructeur>();  
                    LinkedHashSet<Modele> ListModeles =   new LinkedHashSet<Modele>();  
                    
            
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");

            for (Map<String, Object> obj : list) {
                
                Article Article = new Article();
                float id = Float.parseFloat(obj.get("idarticle").toString());
                Article.setIdArticle( (int) id);
                Article.setDesignation(obj.get("designation").toString());
//                Article.setIdCategorie(obj.get("idArticle").toString());
                Article.setImage(obj.get("image").toString());
                Article.setPrixAchat((int)Float.parseFloat(obj.get("prixachat").toString()));
                Article.setPrixVente((int) Float.parseFloat(obj.get("prixvente").toString()));
                Article.setQteStock((int) Float.parseFloat(obj.get("qteStock").toString()));
                Article.setNbReview((int) Float.parseFloat(obj.get("nbReview").toString()));
                Article.setNbLike((int) Float.parseFloat(obj.get("nbFavoris").toString()));
                Article.setDescirption(obj.get("description").toString());
                Modele modele =new Modele();
                Constructeur constructeur=new Constructeur();
                Map<String, Object> ModeleBd =(Map<String, Object>) obj.get("idmodele");
                Map<String, Object> ConstructeurBd =(Map<String, Object>) ModeleBd.get("idconstructeur");
                Map<String, Object> CategorieBd =(Map<String, Object>) obj.get("idcategorie");
                Map<String, Object> FamilleBd =(Map<String, Object>) CategorieBd.get("idfamille");
                
                
                 constructeur.setLibelle((String)ConstructeurBd.get("libelle"));
             
  
               
                modele.setIdConstructeur(constructeur);
                modele.setLibelle((String)ModeleBd.get("libelle"));
             
                
                Famille fam=new Famille(1,(String)FamilleBd.get("libelle"), 1);   
               
                
                Categorie Categorie =new Categorie(fam, (String)CategorieBd.get("libelle"), 1);
                
                Article.setIdCategorie(Categorie);
                Article.setIdModele(modele);
                
                ListFamilles.add(fam);
                ListCategories.add(Categorie);
                ListModeles.add(modele);
                ListConstructeurs.add(constructeur);
                
//                Article.setRaisonSociale(obj.get("raisonsociale").toString());
//                Article.setMatFiscale(obj.get("matfiscale").toString());
//                Article.setNumTel1((int)Float.parseFloat(obj.get("numtel1").toString()));
//                Article.setNumTel2((int)Float.parseFloat(obj.get("numtel2").toString()));
//                Article.setEmail(obj.get("email").toString());
//                Article.setAdresse(obj.get("adresse").toString());
//                Article.setVille(obj.get("ville").toString());
//                Article.setCodePostal((int) Float.parseFloat(obj.get("codepostal").toString()));
//                Article.setPays(obj.get("pays").toString());
//                Article.setDevise(obj.get("devise").toString());
//                Article.setLogo(obj.get("logo").toString());

             
                Articles.add(Article);
                
            }
             cs.setListCategories(ListCategories);
        conserv.setListConstructeurs(ListConstructeurs);
        ms.setListModeles(ListModeles);
        fams.setListFamille(ListFamilles);
        fs.updateData(Articles);

        } catch (IOException ex) {;

        }
            
       
        return Articles;
    }

    public void fetchArticles(){
        String url = Statics.BASE_URL+"/listArticle?idSociete="+Statics.loggedUser.getIdSociete();
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                Articles = parseArticles(new String(req.getResponseData()));
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        this.Articles=Articles;
    }
    
    
    public ArrayList<Article> getallArticles(){
        
        return this.Articles;
        
    }
    
    public Article getArticleFromIdAndFavoris(int id){
        
        for (Article Article1 : Articles) {
            if(Article1.getIdArticle()==id){
                Article1.setFavoris(true);
             return Article1 ;
                
            }
            
        }
     return null;
   
    }
    
     public Article getArticleFromId(int id){
        
        for (Article Article1 : Articles) {
            if(Article1.getIdArticle()==id){
                Article1.setFavoris(true);
             return Article1 ;
                
            }
            
        }
     return null;
   
    }
    
    
    
}
