/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Commande;
import Entites.Livreur;
import Utils.Statics;
import Utils.CurrentSession;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author med talel
 */
public class CommandeService {

    public static CommandeService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Commande> commandes;

    public CommandeService() {

    }

    public static CommandeService getInstance() {
        if (instance == null) {
            instance = new CommandeService();
        }
        return instance;
    }

    public boolean addCommande(Commande t) {
        //String url = Statics.BASE_URL + "/shop/mobileCommande/" + t. //création de l'URL
        //  req.setUrl(url);// Insertion de l'URL de notre demande de connexion
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                req.removeResponseListener(this); //Supprimer cet actionListener
                /* une fois que nous avons terminé de l'utiliser.
                 La ConnectionRequest req est unique pour tous les appels de 
                 n'importe quelle méthode du Service task, donc si on ne supprime
                 pas l'ActionListener il sera enregistré et donc éxécuté même si 
                 la réponse reçue correspond à une autre URL(get par exemple)*/

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return resultOK;
    }

    public ArrayList<Commande> parseCommandes(String jsonText) {
        try {
            commandes = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
            for (Map<String, Object> obj : list) {
                Commande commande = new Commande();
                Livreur livreurObj = new Livreur();
                float id = Float.parseFloat(obj.get("id").toString());
//               Commande.setIdUtilisateur(Statics.loggedUser.getIdUtilisateur());
                commande.setIdCommande((int) id);
                commande.setEtatLivraison(obj.get("EtatLivraison").toString());

                Map<String, Object> livreur = (Map<String, Object>) obj.get("Livreur");
                String livreurImage = (String) livreur.get("image");
                String livreurNom = (String) livreur.get("nom");

                String livreurPrenom = (String) livreur.get("prenom");
                String livreurAdresse = (String) livreur.get("adresse");
                float idlivreur = Float.parseFloat(livreur.get("idlivreur").toString());
                livreurObj.setImage(livreurImage);
                livreurObj.setIdLivreur((int)idlivreur);
                livreurObj.setNom(livreurNom);
                livreurObj.setPrenom(livreurPrenom);
                livreurObj.setAdresse(livreurAdresse);
                commande.setLivreur(livreurObj);
                commandes.add(commande);
            }

        } catch (IOException ex) {

        }
        return commandes;
    }

    public ArrayList<Commande> getAllCommandes() {
        String url = Statics.BASE_URL + "/commandeUtilisateur?idUtilisateur="+Statics.loggedUser.getIdUtilisateur();
        //System.out.println(url);
        ConnectionRequest rq = new ConnectionRequest();
        rq.setPost(false);
        rq.setUrl(url);

        rq.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                commandes = parseCommandes(new String(rq.getResponseData()));
                rq.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(rq);
        return commandes;
    }

}
