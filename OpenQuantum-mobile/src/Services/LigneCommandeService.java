/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Article;
import Entites.Commande;
import Entites.LigneCommade;
import Entites.LigneCommade.LigneCommande;
import Entites.Utilisateur;
import Utils.Statics;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import com.mycompany.myappCo.PanierForm;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

/**
 *
 * @author med talel
 */
public class LigneCommandeService {

    public static LigneCommandeService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Commande> LignesCommande;

    public LigneCommandeService() {
        req = new ConnectionRequest();

    }

    public static LigneCommandeService getInstance() {
        if (instance == null) {
            instance = new LigneCommandeService();
        }
        return instance;
    }

    public boolean ConfirmAchat() {

        String idArticles = "";
        String qtn = "";

        int sizeMap = PanierForm.panierMap.size();
        System.out.println("   MAP SIZE  " + sizeMap);
        int counter = 0;
        for (Entry<Article, Integer> entry : PanierForm.panierMap.entrySet()) {
                idArticles += entry.getKey().getIdArticle() + ",";
                qtn += entry.getValue() + ",";
        }

        System.out.println("idArticles=" + idArticles);
        System.out.println("qtn=" + qtn);

        String url = Statics.BASE_URL + "/confirmAchat";

        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("size", "" + PanierForm.panierMap.size());
        r.addArgument("idArticles", "" + idArticles);
        r.addArgument("qtn", "" + qtn);

        r.addArgument("idSociete", "" + Statics.loggedUser.getIdSociete());
        r.addArgument("idUtilisateur", "" + Statics.loggedUser.getIdUtilisateur());

        r.addArgument("idLivreur", "" + 1);

        ///
        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200; //Code HTTP 200 OK
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }

    

    

}
