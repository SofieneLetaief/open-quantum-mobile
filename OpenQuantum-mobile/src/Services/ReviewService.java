/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Article;
import Entites.Review;
import Entites.Utilisateur;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class ReviewService {
    
       public static ReviewService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public  static Map<Integer,Integer> listRes;
    public ArrayList<Review> reviews;
    int res=-1;
     int test=0;
    public ReviewService() {
        req = new ConnectionRequest();

    }

    public static ReviewService getInstance() {
        if (instance == null) {
            instance = new ReviewService();
        }
        return instance;
    }
    public ArrayList<Review> parseReviews(String jsonText) {
        try {
            reviews = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
            for (Map<String, Object> obj : list) {
                Review review = new Review();
                Utilisateur utilisateurObj = new Utilisateur();
                Article articleObj = new Article();
                float id = Float.parseFloat(obj.get("idreview").toString());
//             )  Commande.setIdUtilisateur(Statics.loggedUser.getIdUtilisateur());
                review.setIdReview((int) id);
                review.setDescription(obj.get("description").toString());
                review.setAddress(obj.get("address").toString());
                review.setDateReview((Date) obj.get("datereview"));
                Map<String, Object> UserBd =(Map<String, Object>) obj.get("idutilisateur");
                float iduser = Float.parseFloat(UserBd.get("idutilisateur").toString());
                utilisateurObj.setIdUtilisateur((int)iduser);
                utilisateurObj.setUsername((String)UserBd.get("username"));
                
                 Map<String, Object> articleBd =(Map<String, Object>) obj.get("idarticle");
                 float idart = Float.parseFloat(articleBd.get("idarticle").toString());
                 articleObj.setIdArticle((int)idart);
                articleObj.setDesignation((String)articleBd.get("designation"));
                
                review.setIdUtilisateur(utilisateurObj);
                review.setIdArticle(articleObj);
                
                reviews.add(review);
            }

        } catch (IOException ex) {

        }
        return reviews;
    }
    public ArrayList<Review> getAllReviews() {
        String url = Statics.BASE_URL + "/listReview?idSociete=5";
        System.out.println(url);
        ConnectionRequest rq = new ConnectionRequest();
        rq.setPost(false);
        rq.setUrl(url);

        rq.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                reviews = parseReviews(new String(rq.getResponseData()));
                rq.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(rq);
        return reviews;
    }
      public int getNbReview(int idArticle) {
             listRes = new HashMap<Integer, Integer>() ;
          int res ;
        String url = Statics.BASE_URL + "/countReview?idArticle=" + idArticle;
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
          
  
          test=parseCountReview(new String(req.getResponseData()));

                        
                 req.removeResponseListener(this);
                
            } 
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        
        return test;
    }
      public boolean addReviewToArticle(Review rev) {

        String url = Statics.BASE_URL + "/setReview";

        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("idArticle", "" + rev.getIdArticle().getIdArticle());
        r.addArgument("description", "" + rev.getDescription());
        r.addArgument("address", "" + rev.getAddress());
        r.addArgument("idUtilisateur", ""+(Statics.loggedUser.getIdUtilisateur()));

        ///
        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200; //Code HTTP 200 OK
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }
          public int  parseCountReview(String jsonText) {
               try {
                    JSONParser j = new JSONParser();
                    Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
                    List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
                 for (Map<String, Object> obj : list) {
              
                    float id = Float.parseFloat(obj.get("nbReview").toString());
                    test=(int)id  ;
                  
                 }
                    
                    
        } catch (IOException ex) {;

        }
              return test;
          }
    
}
