/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.FluxMsg;
import Entites.Utilisateur;
import Utils.CurrentSession;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.ComboBox;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author omar
 */
public class ChatService {

    public static ChatService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Utilisateur> utilisateurs;

    public ChatService() {
        req = new ConnectionRequest();

    }

    public static ChatService getInstance() {
        if (instance == null) {
            instance = new ChatService();
        }
        return instance;
    }

    public ArrayList<Utilisateur> parseUtilisateur(String jsonText) {
        try {
            utilisateurs = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");

            for (Map<String, Object> obj : list) {

                Utilisateur user = new Utilisateur();
                float id = Float.parseFloat(obj.get("idutilisateur").toString());
                user.setIdUtilisateur((int) id);
                user.setNom(obj.get("nom").toString());
                user.setPrenom(obj.get("prenom").toString());
                user.setUsername(obj.get("username").toString());

                utilisateurs.add(user);
            }

        } catch (IOException ex) {;

        }
        return utilisateurs;
    }

    public ArrayList<Utilisateur> getUsersByIdSociete() {
        String url = Statics.BASE_URL + "/getUsersSociete?idsociete="+Statics.loggedUser.getIdSociete();
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                utilisateurs = parseUtilisateur(new String(req.getResponseData()));
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return utilisateurs;
    }

    ArrayList<FluxMsg> msgs, fluxMsgs;

    //retrieving connected user

    public ArrayList<FluxMsg> getMsgs(int idSender, int idReciever) {
        String url = Statics.BASE_URL + "/getMsgs";
        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("idsender", "" + idSender);
        r.addArgument("idreciever", "" + idReciever);

        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                msgs = parseMsgs(new String(r.getResponseData()));
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return msgs;

        ///
    }

    //PARSING MSGS
    public ArrayList<FluxMsg> parseMsgs(String jsonText) {
        try {

            fluxMsgs = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");

            for (Map<String, Object> obj : list) {

                FluxMsg fms = new FluxMsg();
                float idsender = Float.parseFloat(obj.get("idSender").toString());
                float idreciever = Float.parseFloat(obj.get("idReciever").toString());
                float idMsg = Float.parseFloat(obj.get("idMsg").toString());
                String context = obj.get("content").toString();
                fms.setIdSender((int)idsender);
                fms.setIdMsg((int)idMsg);

                fms.setIdReciever((int)idreciever);
                fms.setContent(context);
                String seenval = (obj.get("seen").toString());
                if(seenval.equals("false")){
                    fms.setSeen(false);
                }else{
                    fms.setSeen(true);
                }
                
                fluxMsgs.add(fms);
            }

        } catch (IOException ex) {;

        }
        return fluxMsgs;
    }

    //
    public boolean SendMsgToUser(FluxMsg msg) {

        String url = Statics.BASE_URL + "/sendMsg";

        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("idsender", "" + msg.getIdSender());
        r.addArgument("idreciever", "" + msg.getIdReciever());
        r.addArgument("content", "" + msg.getContent());
        r.addArgument("seen", "" +msg.isSeen());

        ///
        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200; //Code HTTP 200 OK
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }
    
    public boolean MsgSeen(int idMsg) {

        String url = Statics.BASE_URL + "/msgseen";

        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("idmsg", "" + idMsg);
       

        ///
        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200; //Code HTTP 200 OK
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }

}
