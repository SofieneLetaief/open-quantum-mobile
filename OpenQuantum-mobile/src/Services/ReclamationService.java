/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Article;
import Entites.Livraison;
import Entites.Promotion;
import Entites.reclamation;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author hp
 */
public class ReclamationService {
    
    

     public static ReclamationService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<reclamation> Reclamation;
    public ReclamationService() {
        req = new ConnectionRequest();

    }

    public static ReclamationService getInstance() {
        if (instance == null) {
            instance = new ReclamationService();
        }
        return instance;
    
}
 public ArrayList<reclamation> parseReclamation(String jsonText) {
        try {
            Reclamation = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
            for (Map<String, Object> obj : list) {
                reclamation rec = new reclamation();
                float id = Float.parseFloat(obj.get("id").toString());
                rec.setIdrec((int) id);
                rec.setNaturerec(obj.get("naturerec").toString());
                rec.setDescription(obj.get("Description").toString());
                rec.setStat(obj.get("statut").toString());
                Map<String, Object> articleBd =(Map<String, Object>) obj.get("article");
              
                Map<String, Object> livraisonBd =(Map<String, Object>) obj.get("Livraison");
          
                Livraison livraison =new Livraison();
                Article article = new Article();
                article.setDesignation((String)articleBd.get("designation").toString());
                livraison.setTrackingCode((String)livraisonBd.get("trackingcode").toString());
                rec.setIdarticle(article);
                rec.setIdLivraison(livraison);
                Reclamation.add(rec);
                
            }

        } catch (IOException ex) {

        }
        return Reclamation;   
 }
  public boolean addarticlereclamation(reclamation r) {
        String url = Statics.BASE_URL + "/setReclamationArticle";
                
        ConnectionRequest cr = new ConnectionRequest();
        cr.setPost(false);
        cr.setUrl(url);
        
        cr.addArgument("idarticle", "" + r.getIdArticleId());
           cr.addArgument("naturerec", "" + r.getNaturerec());
        cr.addArgument("description", "" + r.getDescription());
        cr.addArgument("image", r.getImage());

                cr.addArgument("idclient", "" + 1);
        cr.addArgument("idlivraison", "" + 2);
        cr.addArgument("idutilisateur", "" + Statics.loggedUser.getIdUtilisateur());

        cr.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = cr.getResponseCode() == 200; //Code HTTP 200 OK
                cr.removeResponseListener(this); 

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(cr);
        return resultOK;
    }
  
   public boolean delete(int id ) {
        String url = Statics.BASE_URL + "/deleteReclamationArticle?idRec=" + id;
        req.setUrl(url);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = req.getResponseCode() == 200;
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return resultOK;
    }
  
  
   public ArrayList<reclamation> getAllReclamation() {
        String url = Statics.BASE_URL + "/listReclamation";
        //System.out.println(url);
        ConnectionRequest rq = new ConnectionRequest();
        rq.setPost(false);
        rq.setUrl(url);

        rq.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                Reclamation = parseReclamation(new String(rq.getResponseData()));
                rq.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(rq);
        return Reclamation;
    }
}
