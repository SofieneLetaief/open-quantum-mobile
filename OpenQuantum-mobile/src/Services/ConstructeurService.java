/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Constructeur;
import com.codename1.io.ConnectionRequest;
import java.util.LinkedHashSet;

/**
 *
 * @author Letaief Sofiene
 */

public class ConstructeurService {
             private LinkedHashSet<Constructeur> ListConstructeurs =   new LinkedHashSet<Constructeur>();  
        private ConnectionRequest req;
           public static ConstructeurService instance = null;
    
        public ConstructeurService() {
        req = new ConnectionRequest();

    }

    public static ConstructeurService getInstance() {
        if (instance == null) {
            instance = new ConstructeurService();
        }
        return instance;
    }
    
      public LinkedHashSet<Constructeur> getConstructeurs(){
        return this.ListConstructeurs;
        
    } 
    public void setListConstructeurs(LinkedHashSet<Constructeur> LC){
    this.ListConstructeurs=LC;
    }
}
