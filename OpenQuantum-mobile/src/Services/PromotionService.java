/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Services;


import Entites.Promotion;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Mannai
 */
public class PromotionService {
    

     public static PromotionService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Promotion> promotions;
    int res=-1;
     int test=0;
    public PromotionService() {
        req = new ConnectionRequest();

    }

    public static PromotionService getInstance() {
        if (instance == null) {
            instance = new PromotionService();
        }
        return instance;
    
}
    public ArrayList<Promotion> parsePromotions(String jsonText) {
        try {
            promotions = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
            for (Map<String, Object> obj : list) {
                Promotion promo = new Promotion();
                float id = Float.parseFloat(obj.get("idpourcentage").toString());
                promo.setIdPourcentage((int) id);
                promo.setCode(obj.get("code").toString());
                float pourcentage = Float.parseFloat(obj.get("pourcentage").toString());
                promo.setPoucentage((int)pourcentage);
                
                promotions.add(promo);
            }

        } catch (IOException ex) {

        }
        return promotions;
    }
    public ArrayList<Promotion> getAllPromotions() {
        String url = Statics.BASE_URL + "/listPromotion?idsociete=1";
        System.out.println(url);
        ConnectionRequest rq = new ConnectionRequest();
        rq.setPost(false);
        rq.setUrl(url);

        rq.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                promotions = parsePromotions(new String(rq.getResponseData()));
                rq.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(rq);
        return promotions;
    }
}
