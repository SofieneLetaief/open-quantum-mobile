/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Article;
import Entites.Utilisateur;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class FavorisService {

    public static FavorisService instance = null;
    private LinkedHashSet<Article> listArticleFavoris = new LinkedHashSet<Article>();

    public boolean resultOK;
    private ConnectionRequest req;
    public static Map<Integer, Integer> listRes;

    int test = 0;

    public FavorisService() {
        req = new ConnectionRequest();

    }

    public static FavorisService getInstance() {
        if (instance == null) {
            instance = new FavorisService();
        }
        return instance;
    }

    public int getNbFavoris(int idArticle) {
        listRes = new HashMap<Integer, Integer>();
        int res;
        String url = Statics.BASE_URL + "/countFavoris?idArticle=" + idArticle;
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {

                test = parseCountFavoris(new String(req.getResponseData()));

                req.removeResponseListener(this);

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);

        return test;
    }

    public int parseCountFavoris(String jsonText) {
        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
            for (Map<String, Object> obj : list) {

                float id = Float.parseFloat(obj.get("nbFavoris").toString());
                test = (int) id;

            }

        } catch (IOException ex) {;

        }
        return test;
    }

    public LinkedHashSet<Article> getArticleFavoris() {

        listRes = new HashMap<Integer, Integer>();
        int res;
        String url = Statics.BASE_URL + "/listFavoris?idUtilisateur=" + Statics.loggedUser.getIdUtilisateur();
        req.setUrl(url);
        req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                getListFavorisFromAllArticles(new String(req.getResponseData()));
                req.removeResponseListener(this);

            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);

        return this.listArticleFavoris;
    }

    public void getListFavorisFromAllArticles(String jsonText) {
        ArticleService as = ArticleService.getInstance();

        try {
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));
            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");
            for (Map<String, Object> obj : list) {

                float id = Float.parseFloat(obj.get("idArticleFav").toString());
                test = (int) id;
                listArticleFavoris.add(as.getArticleFromIdAndFavoris(test));

            }

        } catch (IOException ex) {;

        }

    }

    public boolean addArticleFavoris(int id) {

        String url = Statics.BASE_URL + "/setFavoris";
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(true);
        r.setUrl(url);
        r.addArgument("idArticle", String.valueOf(id));
        r.addArgument("idUtilisateur", String.valueOf(Statics.loggedUser.getIdUtilisateur()));

        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200;
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }

    public boolean removeArticleFavoris(int id) {

        String url = Statics.BASE_URL + "/deleteFavoris";
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(true);
        r.setUrl(url);
        r.addArgument("idArticle", String.valueOf(id));

        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200;
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }

    public LinkedHashSet<Article> getListArticleFavoris() {
        return listArticleFavoris;
    }

    public void setListArticleFavoris(LinkedHashSet<Article> listArticleFavoris) {
        this.listArticleFavoris = listArticleFavoris;
    }
    
    public void ajoutArticleToList(Article art){
    listArticleFavoris.add(art);
    }
     
    public void deleteArticleToList(Article art){
    listArticleFavoris.remove(art);
    }
    
    
    

}
