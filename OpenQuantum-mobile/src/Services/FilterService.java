/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Article;
import Entites.Categorie;
import static Services.ArticleService.instance;
import com.codename1.io.ConnectionRequest;
import java.util.ArrayList;
import java.util.Map;

/**
 *
 * @author Letaief Sofiene
 */
public class FilterService {

    public static FilterService instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public static Map<Integer, Integer> listRes;
    public ArrayList<Article> Articles;
    public int max;
    public int min;

    private ArrayList<String> categories = new ArrayList<>();
    private ArrayList<String> models = new ArrayList<>();

    public FilterService() {
        req = new ConnectionRequest();
        Articles = new ArrayList<>();
        categories = new ArrayList<>();
        models = new ArrayList<>();
        min = 0;
        max = 0;

    }

    public static FilterService getInstance() {
        if (instance == null) {
            instance = new FilterService();
        }
        return instance;
    }

    public void updateData(ArrayList<Article> Articles) {
        this.Articles = Articles;
    }

    public void resetFilter() {
        Articles = new ArrayList<>();
        categories = new ArrayList<>();
        models = new ArrayList<>();
        min = 0;
        max = 0;
    }

    public ArrayList<Article> filterByKey(String mot) {
        if ("".equals(mot)) {
            return Articles;

        } else {
            ArrayList<Article> newArticle = new ArrayList<>();
            for (Article article : Articles) {
                if (article.getDesignation().toLowerCase().equals(mot.toLowerCase())) {
                    newArticle.add(article);
                }

            }
            return newArticle;
        }
    }

    public ArrayList<Article> filterByCategorie() {
        ArrayList<Article> newArticle = new ArrayList<>();
        if (categories.isEmpty()) {
            return Articles;
        } else {
            for (Article article : Articles) {
                for (String cate : categories) {
                    if (article.getIdCategorie().getLibelle().toLowerCase().equals(cate.toLowerCase())) {
                        newArticle.add(article);
                    }
                }
            }
            return newArticle;
        }
    }
    
     public ArrayList<Article> filterByCategorie(Categorie cate) {
        ArrayList<Article> newArticle = new ArrayList<>();
       
            for (Article article : Articles) {
             
                    if (article.getIdCategorie().getLibelle().toLowerCase().equals(cate.getLibelle().toLowerCase())) {
                        newArticle.add(article);
                  
                }
            }
            return newArticle;
        
    }
    
    

    public ArrayList<Article> filterByPrice() {
        ArrayList<Article> newArticle = new ArrayList<>();
        if (min == 0 && max == 0) {
            return Articles;
        } else {
            for (Article article : Articles) {
                if (article.getPrixVente() <= max && article.getPrixVente() >= min) {
                    newArticle.add(article);
                }
            }
            return newArticle;
        }

    }
//        return Articles.stream()
//                .filter((e) -> {
//                    
//                        boolean res=false;
//                        for (String cate : categories) {
//                            if(cate.toLowerCase().equals(e.getIdCategorie().getLibelle().toLowerCase())){
//                            res=true;
//                            }
//                            
//                        }
//                       return res;
//                })
//                .collect(Collectors.toCollection(ArrayList::new));

    public ArrayList<Article> filterByModele() {
        ArrayList<Article> newArticle = new ArrayList<>();
        if (categories.isEmpty()) {
            return Articles;
        } else {
            for (Article article : Articles) {
                for (String model : models) {
                    if (article.getIdModele().getLibelle().toLowerCase().equals(model.toLowerCase())) {
                        newArticle.add(article);
                    }
                }
            }
            return newArticle;
        }
    }

    public ArrayList<String> getImagesArticles(Categorie cat) {
        ArrayList<String> newArticle = new ArrayList<>();
        int i = 0;
        for (Article article : Articles) {
            
            if (article.getIdCategorie().equals(cat)) {
                newArticle.add(article.getImage());
                i++;
            }
            if (i == 3) {
                break;
            }

        }
        return newArticle;
    }

//
//        return Articles.stream()
//                .filter((e) -> {
//                    
//                        boolean res=false;
//                        for (String cate : categories) {
//                            if(cate.toLowerCase().equals(e.getIdModele().getLibelle().toLowerCase())){
//                            res=true;
//                            }
//                            
//                        }
//                       return res;
//                })
//                .collect(Collectors.toCollection(ArrayList::new));
    public void addCategorie(String cate) {
        categories.add(cate);
    }

    public void addModel(String model) {
        models.add(model);
    }

    public void deleteCategorie(String cate) {
        categories.remove(cate);
    }

    public void deleteModel(String model) {
        categories.remove(model);
    }

}
