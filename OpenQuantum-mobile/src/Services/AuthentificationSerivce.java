/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Utilisateur;
import Utils.Statics;
import com.codename1.io.CharArrayReader;
import com.codename1.io.ConnectionRequest;
import com.codename1.io.JSONParser;
import com.codename1.io.NetworkEvent;
import com.codename1.io.NetworkManager;
import com.codename1.ui.events.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author HP OMEN
 */
public class AuthentificationSerivce {

    public static AuthentificationSerivce instance = null;
    public boolean resultOK;
    private ConnectionRequest req;
    public ArrayList<Utilisateur> users;

    public AuthentificationSerivce() {
        req = new ConnectionRequest();

    }

    public static AuthentificationSerivce getInstance() {
        if (instance == null) {
            instance = new AuthentificationSerivce();
        }
        return instance;
    }

    //Checking login User
    public boolean checkLogin(String username, String password) {
        String url = Statics.BASE_URL + "/loginUser/" + username + "/" + password;
        req.setUrl(url);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = req.getResponseCode() == 200; //Code HTTP 200 OK
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return resultOK;
    }

    //Parsing user 
    public ArrayList<Utilisateur> parseUsers(String jsonText) {
        try {
            users = new ArrayList<>();
            JSONParser j = new JSONParser();
            Map<String, Object> tasksListJson = j.parseJSON(new CharArrayReader(jsonText.toCharArray()));

            List<Map<String, Object>> list = (List<Map<String, Object>>) tasksListJson.get("root");

            for (Map<String, Object> obj : list) {
                Utilisateur user = new Utilisateur();
                float id = Float.parseFloat(obj.get("id").toString());
                user.setIdUtilisateur((int) id);
                //user.setStatus(((int)Float.parseFloat(obj.get("status").toString())));
                user.setNom(obj.get("nom").toString());
                user.setPrenom(obj.get("prenom").toString());
                float numtel = Float.parseFloat(obj.get("numTel").toString());
                user.setNumTel((int) numtel);
                user.setEmail(obj.get("email").toString());
                float idsociete = Float.parseFloat(obj.get("idSociete").toString());
                user.setIdSociete((int) idsociete);
                users.add(user);
            }

        } catch (IOException ex) {;

        }
        return users;
    }

    //retrieving connected user
    public ArrayList<Utilisateur> getUser(String username, String password) {
        String url = Statics.BASE_URL + "/loginUser/" + username + "/" + password;
        req.setUrl(url);
        // req.setPost(false);
        req.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                users = parseUsers(new String(req.getResponseData()));
                req.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(req);
        return users;
    }

    public boolean addUserToSciete(Utilisateur user) {

        String url = Statics.BASE_URL + "/createUserJava";

        ///
        ConnectionRequest r = new ConnectionRequest();
        r.setPost(false);
        r.setUrl(url);

        r.addArgument("idSociete", "" + user.getIdSociete());
        r.addArgument("nom", "" + user.getNom());
        r.addArgument("prenom", "" + user.getPrenom());
        r.addArgument("email", "" + user.getEmail());
        r.addArgument("numtel", "" + user.getNumTel());
        r.addArgument("username", "" + user.getUsername());
        r.addArgument("plainPassword", "" + user.getPassword());

        ///
        r.addResponseListener(new ActionListener<NetworkEvent>() {
            @Override
            public void actionPerformed(NetworkEvent evt) {
                resultOK = r.getResponseCode() == 200; //Code HTTP 200 OK
                r.removeResponseListener(this);
            }
        });
        NetworkManager.getInstance().addToQueueAndWait(r);
        return resultOK;
    }

}
