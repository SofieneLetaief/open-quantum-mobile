/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import Entites.Modele;
import com.codename1.io.ConnectionRequest;
import java.util.LinkedHashSet;

/**
 *
 * @author Letaief Sofiene
 */
public class ModeleService {
             private LinkedHashSet<Modele> ListModeles =   new LinkedHashSet<Modele>();  
        private ConnectionRequest req;
           public static ModeleService instance = null;
    
        public ModeleService() {
        req = new ConnectionRequest();

    }

    public static ModeleService getInstance() {
        if (instance == null) {
            instance = new ModeleService();
        }
        return instance;
    }
    
      public LinkedHashSet<Modele> getModeles(){
        return this.ListModeles;
        
    } 
    public void setListModeles(LinkedHashSet<Modele> LC){
    this.ListModeles=LC;
    }
    
}
