/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;




/**
 *
 * @author Letaief Sofiene
 */
public class Categorie {

    private int idCategorie;
    private Famille idFamille;
    private String libelle;
    private int active;

    public Categorie(int idCategorie, Famille idFamille, String libelle, int active) {
        this.idCategorie = idCategorie;
        this.idFamille = idFamille;
        this.libelle = libelle;
        this.active = active;
    }

    public Categorie(Famille idFamille, String libelle, int active) {
        this.idFamille = idFamille;
        this.libelle = libelle;
        this.active = active;
    }

    public int getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(int idCategorie) {
        this.idCategorie = idCategorie;
    }

    public Famille getIdFamille() {
        return idFamille;
    }

    public void setIdFamille(Famille idFamille) {
        this.idFamille = idFamille;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Categorie{" + "idCategorie=" + idCategorie + ", idFamille=" + idFamille.toString() + ", libelle=" + libelle + ", active=" + active + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.idCategorie;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Categorie other = (Categorie) obj;
       
        if (!(this.libelle.equals(other.libelle))) {
            return false;
        }
        if (!(this.idFamille.equals(other.idFamille))) {
            return false;
        }
        return true;
    }

 

    
    
}
