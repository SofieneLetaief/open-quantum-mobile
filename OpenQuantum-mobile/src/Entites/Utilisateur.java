/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;

/**
 *
 * @author HP OMEN
 */
public class Utilisateur {

    // MEZAL BECH YETBADEL BA3EED KI BECH NZIDOUH 7AJET JDOD 
    int idUtilisateur, numTel, idSociete;
    String nom, prenom, email;
    String username,password;

    public Utilisateur() {

    }

    
    public Utilisateur(int idUtilisateur, int numTel, int idSociete, String nom, String prenom, String email) {
        this.idUtilisateur = idUtilisateur;
        this.numTel = numTel;
        this.idSociete = idSociete;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
    }

    public Utilisateur(int idSociete, String nom, String prenom, String email, int numTel, String username, String password) {
        this.numTel = numTel;
        this.idSociete = idSociete;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    

    

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getNumTel() {
        return numTel;
    }

    public void setNumTel(int numTel) {
        this.numTel = numTel;
    }

    public int getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Utilisateur{" + "idUtilisateur=" + idUtilisateur + ", numTel=" + numTel + ", idSociete=" + idSociete + ", nom=" + nom + ", prenom=" + prenom + ", email=" + email + ", username=" + username + '}';
    }
    
    

   
}
