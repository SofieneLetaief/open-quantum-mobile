/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;


/**
 *
 * @author hp
 */
public class reclamation {
   private String naturerec,description,stat;
   private int idrec;
   private Article idarticle;
   private Livraison   idLivraison;
   private int idclient,idar;
   public String image;
   
   
   public reclamation(){
        
        
    }

    public reclamation(String naturerec, String description, Article idarticle) {
        this.naturerec = naturerec;
        this.description = description;
        this.idarticle = idarticle;
    }
   

    public reclamation(String naturerec, String description, int idrec, Article idarticle, Livraison idLivraison, int idclient) {
        this.naturerec = naturerec;
        this.description = description;
        this.idrec = idrec;
        this.idarticle = idarticle;
        this.idLivraison = idLivraison;
        this.idclient = idclient;
    }

    public reclamation(int idar, String stat, String text,String img) {
         this.idar=idar;
         this.naturerec=stat;
         this.description=text;
         this.image=img;
    }

    public String getNaturerec() {
        return naturerec;
    }

    public void setNaturerec(String naturerec) {
        this.naturerec = naturerec;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public int getIdrec() {
        return idrec;
    }

    public void setIdrec(int idrec) {
        this.idrec = idrec;
    }

    public Article getIdarticle() {
        return idarticle;
    }
    
    
    public int getIdArticleId(){
    return idar;
    }
    public void setIdarticle(Article idarticle) {
        this.idarticle = idarticle;
    }

    public Livraison getIdLivraison() {
        return idLivraison;
    }

    public void setIdLivraison(Livraison idLivraison) {
        this.idLivraison = idLivraison;
    }

    public int getIdclient() {
        return idclient;
    }

    public void setIdclient(int idclient) {
        this.idclient = idclient;
    }

    @Override
    public String toString() {
        return "reclamation{" + "naturerec=" + naturerec + ", description=" + description + ", idrec=" + idrec + '}';
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    
    

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + this.idrec;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final reclamation other = (reclamation) obj;
        return true;
    }

    
   
}

