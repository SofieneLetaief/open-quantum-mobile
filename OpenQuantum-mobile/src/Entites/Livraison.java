/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;

import java.util.Date;

/**
 *
 * @author HP OMEN
 */
public class Livraison {
    
    private int idLivraison,idSociete,idUtilisateur;
    private String trackingCode;
    private Date dateCreation,dateSortie;
    private String status;
    
    private Livreur livreur;

    
    public Livraison(){
        
        
    }
    
    
    public Livraison(int idLivraison, int idSociete, int idUtilisateur, String trackingCode, Date dateCreation, Date dateSortie, String status, Livreur livreur) {
        this.idLivraison = idLivraison;
        this.idSociete = idSociete;
        this.idUtilisateur = idUtilisateur;
        this.trackingCode = trackingCode;
        this.dateCreation = dateCreation;
        this.dateSortie = dateSortie;
        this.status = status;
        this.livreur = livreur;
    }

    
    
    
    @Override
    public String toString() {
        return "Livraison{" + "idLivraison=" + idLivraison + ", idSociete=" + idSociete + ", idUtilisateur=" + idUtilisateur + ", trackingCode=" + trackingCode + ", dateCreation=" + dateCreation + ", dateSortie=" + dateSortie + ", status=" + status + ", livreur=" + livreur + '}';
    }

        
    
    
    
    public int getIdLivraison() {
        return idLivraison;
    }

    public void setIdLivraison(int idLivraison) {
        this.idLivraison = idLivraison;
    }

    public int getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getTrackingCode() {
        return trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        this.trackingCode = trackingCode;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Livreur getLivreur() {
        return livreur;
    }

    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }
    
    
    
    
    
    
}
