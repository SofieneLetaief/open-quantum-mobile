/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;

/**
 *
 * @author Mannai
 */
public class Promotion {

    private int idPourcentage;
    private String code;
    private int poucentage;

    public Promotion() {
    }

    
    public Promotion(int idPourcentage, String code, int poucentage) {
        this.idPourcentage = idPourcentage;
        this.code = code;
        this.poucentage = poucentage;
    }

    public int getIdPourcentage() {
        return idPourcentage;
    }

    public void setIdPourcentage(int idPourcentage) {
        this.idPourcentage = idPourcentage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getPoucentage() {
        return poucentage;
    }

    public void setPoucentage(int poucentage) {
        this.poucentage = poucentage;
    }

    @Override
    public String toString() {
        return "Promotion{" + "idPourcentage=" + idPourcentage + ", code=" + code + ", poucentage=" + poucentage + '}';
    }

    
   
}
