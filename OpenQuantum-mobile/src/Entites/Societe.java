/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entites;

/**
 *
 * @author med talel
 */
public class Societe {
    
    private int idSociete ;
    private String raisonSociale;
    private String matFiscale;
    private int numTel1;
    private int numTel2;
    private String email;
    private String adresse;
    private String ville;
    private int codePostal;
    private String pays;
    private String devise;
    private String logo;
    private int active; 

    public Societe(){
        
    }
    public Societe(int idSociete, String raisonSociale, String matFiscale, int numTel1, int numTel2, String email, String adresse, String ville, int codePostal, String pays, String devise, String logo, int active) {
        this.idSociete = idSociete;
        this.raisonSociale = raisonSociale;
        
        this.matFiscale = matFiscale;
        this.numTel1 = numTel1;
        this.numTel2 = numTel2;
        this.email = email;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostal;
        this.pays = pays;
        this.devise = devise;
        this.logo = logo;
        this.active = active;
    }

    public Societe(String raisonSociale, String matFiscale, int numTel1, int numTel2, String email, String adresse, String ville, int codePostal, String pays, String devise, String logo, int active) {
        this.raisonSociale = raisonSociale;
        this.matFiscale = matFiscale;
        this.numTel1 = numTel1;
        this.numTel2 = numTel2;
        this.email = email;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostal;
        this.pays = pays;
        this.devise = devise;
        this.logo = logo;
        this.active = active;
    }

    
    public int getIdSociete() {
        return idSociete;
    }

    public String getRaisonSociale() {
        return raisonSociale;
    }

    public String getMatFiscale() {
        return matFiscale;
    }

    public int getNumTel1() {
        return numTel1;
    }

    public int getNumTel2() {
        return numTel2;
    }

    public String getEmail() {
        return email;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getVille() {
        return ville;
    }

    public int getCodePostal() {
        return codePostal;
    }

    public String getPays() {
        return pays;
    }

    public String getDevise() {
        return devise;
    }

    public String getLogo() {
        return logo;
    }

    public int getActive() {
        return active;
    }

    public void setRaisonSociale(String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }

    public void setMatFiscale(String matFiscale) {
        this.matFiscale = matFiscale;
    }

    public void setNumTel1(int numTel1) {
        this.numTel1 = numTel1;
    }

    public void setNumTel2(int numTel2) {
        this.numTel2 = numTel2;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public void setDevise(String devise) {
        this.devise = devise;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }
 
     
    @Override
    public String toString() {
        return "Societe{" + "idSociete=" + idSociete + ", raisonSociale=" + raisonSociale + ", matFiscale=" + matFiscale + ", numTel1=" + numTel1 + ", numTel2=" + numTel2 + ", email=" + email + ", adresse=" + adresse + ", ville=" + ville + ", codePostal=" + codePostal + ", pays=" + pays + ", devise=" + devise + ", logo=" + logo + ", active=" + active + '}'+"\n";
    }
    
    
    
      
}

