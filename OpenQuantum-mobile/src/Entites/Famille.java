/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;



/**
 *
 * @author Letaief Sofiene
 */
public class Famille {

    private int idFamille;
    private int idSociete;
    private String libelle;
    private int active;

    public Famille(){
        
    }
    public Famille(int idFamille, int idSociete, String libelle, int active) {
        this.idFamille = idFamille;
        this.idSociete = idSociete;
        this.libelle = libelle;
        this.active = active;
    }

    public Famille(int idSociete, String libelle, int active) {
        this.idSociete = idSociete;
        this.libelle = libelle;
        this.active = active;
    }

    public int getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }

    public int getIdFamille() {
        return idFamille;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getActive() {
        return active;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Famille{" + "idFamille=" + idFamille + ", idSociete=" + idSociete + ", libelle=" + libelle + ", active=" + active + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + this.idFamille;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Famille other = (Famille) obj;
      
        if (!(this.libelle.equals(other.libelle))) {
            return false;
        }
        return true;
    }
    

    public void setIdFamille(int idFamille) {
        this.idFamille = idFamille;
    }
    

}
