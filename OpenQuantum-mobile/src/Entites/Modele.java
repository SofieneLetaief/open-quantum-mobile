/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;


/**
 *
 * @author Letaief Sofiene
 */
public class Modele {
    
    private int idModele;
    private Constructeur idConstructeur;
    private String libelle; 
    private int active; 
    
    public Modele(){
        
    }

    public Modele(int idModele, Constructeur idConstructeur, String libelle, int active) {
        this.idModele = idModele;
        this.idConstructeur = idConstructeur;
        this.libelle = libelle;
        this.active = active;
    }

    public Modele(Constructeur idConstructeur, String libelle, int active) {
        this.idConstructeur = idConstructeur;
        this.libelle = libelle;
        this.active = active;
    }

    public Constructeur getIdConstructeur() {
        return idConstructeur;
    }

    public void setIdConstructeur(Constructeur idConstructeur) {
        this.idConstructeur = idConstructeur;
    }

    
  

    public int getIdModele() {
        return idModele;
    }

    public String getLibelle() {
        return libelle;
    }

    public int getActive() {
        return active;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Modele{" + "idModele=" + idModele + ", idConstructeur=" + idConstructeur + ", libelle=" + libelle + ", active=" + active + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + this.idModele;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Modele other = (Modele) obj;
         if (!this.idConstructeur.equals(other.idConstructeur)) {
            return false;
        }
        if (!this.libelle.equals(other.libelle)) {
            return false;
        }
        return true;
    }
    

 
    
    
    
    
}
