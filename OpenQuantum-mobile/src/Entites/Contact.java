/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;

/**
 *
 * @author omar
 */
public class Contact {
    
    
    private int id;
    private String nom;
    private String prenom;
    private int NumTel;
    private String email;
    private String image;

    public Contact(int id, String nom, String prenom, int NumTel, String email, String image) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.NumTel = NumTel;
        this.email = email;
        this.image = image;
    }

    public Contact() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public int getNumTel() {
        return NumTel;
    }

    public void setNumTel(int NumTel) {
        this.NumTel = NumTel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Contact{" + "id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", NumTel=" + NumTel + ", email=" + email + ", image=" + image + '}';
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contact other = (Contact) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
    
    
}
