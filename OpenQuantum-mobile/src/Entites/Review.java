/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entites;

import java.util.Date;

/**
 *
 * @author Mannai
 */
public class Review {
    private int idReview;
    private String description;
    private String address;
    private Date dateReview;
    private int active;
    private Article idArticle;
    private Utilisateur idUtilisateur;

    public Review() {
    }

    public Review(int idReview, String description, String address,Date dateReview, int active, Article idArticle, Utilisateur idUtilisateur) {
        this.idReview = idReview;
        this.description = description;
        this.address = address;
        this.dateReview = dateReview;
        this.active = active;
        this.idArticle = idArticle;
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdReview() {
        return idReview;
    }

    public void setIdReview(int idReview) {
        this.idReview = idReview;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateReview() {
        return dateReview;
    }

    public void setDateReview(Date dateReview) {
        this.dateReview = dateReview;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public Article getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(Article idArticle) {
        this.idArticle = idArticle;
    }

    public Utilisateur getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(Utilisateur idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    @Override
    public String toString() {
        return "Review{" + "idReview=" + idReview + ", description=" + description + ", address=" + address + ", dateReview=" + dateReview + ", active=" + active + ", idArticle=" + idArticle + ", idUtilisateur=" + idUtilisateur + '}';
    }

   

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.idReview;
        return hash;
    }

}
