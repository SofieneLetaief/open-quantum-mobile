/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entites;



/**
 *
 * @author med talel
 */
public class Constructeur {
    private int idConstructeur;
    private int idSociete;
    private String libelle;
    private int active;

    public Constructeur() {
    }

    public Constructeur(int idConstructeur, int idSociete, String libelle, int active) {
        this.idConstructeur = idConstructeur;
        this.idSociete = idSociete;
        this.libelle = libelle;
        this.active = active;
    }

    public Constructeur(int idConstructeur, String libelle) {
        this.idConstructeur = idConstructeur;
        this.libelle = libelle;
    }
    
    

    public Constructeur(int idSociete, String libelle, int active) {
        this.idSociete = idSociete;
        this.libelle = libelle;
        this.active = active;
    }

    public int getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }
  

    

    public int getIdConstructeur() {
        return idConstructeur;
    }

   

    public String getLibelle() {
        return libelle;
    }

    public int getActive() {
        return active;
    }

   

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Constructeur{" + "idConstructeur=" + idConstructeur + ", idSociete=" + idSociete + ", libelle=" + libelle + ", active=" + active + '}';
    }

    public void setIdConstructeur(int idConstructeur) {
        this.idConstructeur = idConstructeur;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Constructeur other = (Constructeur) obj;
       
        if (!this.libelle.equals(other.libelle)) {
            return false;
        }
        return true;
    }
    
    


    
    
}
