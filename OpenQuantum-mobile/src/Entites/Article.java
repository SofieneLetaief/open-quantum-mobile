/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;



/**
 *
 * @author Letaief Sofiene
 */
public class Article {
    
    
    private int idArticle;
    private int idUnite;
    private Modele modele;
    private int idTaxe;
    private Categorie categorie;
    private int idSociete;
    private String refInterne;
    private String reffourn;
    private String designation;
    private int prixAchat;
    private int prixVente;
    private String image;
    private int codeBarre;
    private int active;
    private int qteStock;
    private int nbReview;
    private int nbLike;
    private String Descirption;
    private boolean favoris;

    public Article() {
    }

    public Article(int idArticle, int idUnite, Modele modele, int idTaxe, Categorie categorie, int idSociete, String refInterne, String reffourn, String designation, int prixAchat, int prixVente, String image, int codeBarre, int active) {
        this.idArticle = idArticle;
        this.idUnite = idUnite;
        this.modele = modele;
        this.idTaxe = idTaxe;
        this.categorie = categorie;
        this.idSociete = idSociete;
        this.refInterne = refInterne;
        this.reffourn = reffourn;
        this.designation = designation;
        this.prixAchat = prixAchat;
        this.prixVente = prixVente;
        this.image = image;
        this.codeBarre = codeBarre;

        this.active = active;
    }
    public Article(int idUnite, Modele modele, int idTaxe, Categorie categorie, int idSociete, String refInterne, String reffourn, String designation, int prixAchat, int prixVente, String image, int codeBarre, int active) {
        this.idUnite = idUnite;
        this.modele = modele;
        this.idTaxe = idTaxe;
        this.categorie = categorie;
        this.idSociete = idSociete;
        this.refInterne = refInterne;
        this.reffourn = reffourn;
        this.designation = designation;
        this.prixAchat = prixAchat;
        this.prixVente = prixVente;
        this.image = image;
        this.codeBarre = codeBarre;
        this.active = active;
    }

    public int getIdUnite() {
        return idUnite;
    }

    public void setIdUnite(int idUnite) {
        this.idUnite = idUnite;
    }

    public Modele getIdModele() {
        return modele;
    }

    public void setIdModele(Modele modele) {
        this.modele = modele;
    }

    public int getIdTaxe() {
        return idTaxe;
    }

    public void setIdTaxe(int idTaxe) {
        this.idTaxe = idTaxe;
    }

    public Categorie getIdCategorie() {
        return categorie;
    }

    public void setIdCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public int getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

   
    
    

    public int getIdArticle() {
        return idArticle;
    }

    public String getRefInterne() {
        return refInterne;
    }

    public String getReffourn() {
        return reffourn;
    }

    public String getDesignation() {
        return designation;
    }

    public int getPrixAchat() {
        return prixAchat;
    }

    public int getPrixVente() {
        return prixVente;
    }

    public String getImage() {
        return image;
    }

    public int getCodeBarre() {
        return codeBarre;
    }

    public int getActive() {
        return active;
    }

    public void setRefInterne(String refInterne) {
        this.refInterne = refInterne;
    }

    public void setReffourn(String reffourn) {
        this.reffourn = reffourn;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setPrixAchat(int prixAchat) {
        this.prixAchat = prixAchat;
    }

    public void setPrixVente(int prixVente) {
        this.prixVente = prixVente;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }

    public void setActive(int active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "Article{" + "idArticle=" + idArticle + ", idUnite=" + idUnite + ", refInterne=" + refInterne + ", reffourn=" + reffourn + ", designation=" + designation + ", prixAchat=" + prixAchat + ", prixVente=" + prixVente + ", image=" + image + ", codeBarre=" + codeBarre + ", active=" + active + ", qteStock=" + qteStock + ", nbReview=" + nbReview + ", nbLike=" + nbLike + ", cate=" + categorie.toString()+ ", model=" + modele.toString() +'}'+"\n";
    }

  

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.idArticle;
        return hash;
    }


//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final Article other = (Article) obj;
//        if (this.idArticle != other.idArticle) {
//            return false;
//        }
//        if (this.idUnite != other.idUnite) {
//            return false;
//        }
//        if (this.modele != other.modele) {
//            return false;
//        }
//        if (this.idTaxe != other.idTaxe) {
//            return false;
//        }
//        if (this.categorie != other.categorie) {
//            return false;
//        }
//        if (this.idSociete != other.idSociete) {
//            return false;
//        }
//        if (!Objects.equals(this.refInterne, other.refInterne)) {
//            return false;
//        }
//        if (!Objects.equals(this.reffourn, other.reffourn)) {
//            return false;
//        }
//        if (!Objects.equals(this.designation, other.designation)) {
//            return false;
//        }
//        if (this.prixAchat != other.prixAchat) {
//            return false;
//        }
//        if (this.prixVente != other.prixVente) {
//            return false;
//        }
//        if (!Objects.equals(this.image, other.image)) {
//            return false;
//        }
//        if (this.codeBarre != other.codeBarre) {
//            return false;
//        }
//        if (this.active != other.active) {
//            return false;
//        }
//        return true;
//    }

    public int getQteStock() {
        return qteStock;
    }

    public void setQteStock(int qteStock) {
        this.qteStock = qteStock;
    }

    public int getNbReview() {
        return nbReview;
    }

    public void setNbReview(int nbReview) {
        this.nbReview = nbReview;
    }

    public int getNbLike() {
        return nbLike;
    }

    public void setNbLike(int nbLike) {
        this.nbLike = nbLike;
    }

    public Modele getModele() {
        return modele;
    }

    public void setModele(Modele modele) {
        this.modele = modele;
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public String getDescirption() {
        return Descirption;
    }

    public void setDescirption(String Descirption) {
        this.Descirption = Descirption;
    }

    public boolean isFavoris() {
        return favoris;
    }

    public void setFavoris(boolean favoris) {
        this.favoris = favoris;
    }  
    
}
