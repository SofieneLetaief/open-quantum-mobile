/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entites;

import java.util.Date;



/**
 *
 * @author med talel
 */
public class Commande {
    
    private int idCommande,idUtilisateur,idSociete,idLivreur;
    private Date date;
    private String etatLivraison;
    private Livreur livreur;
    private Article article; 
    
    public Commande(){
        
    }

    public Commande(int idCommande, int idUtilisateur, int idSociete, int idLivreur, Date date, String etatLivraison, Livreur livreur,Article article) {
        this.idCommande = idCommande;
        this.idUtilisateur = idUtilisateur;
        this.idSociete = idSociete;
        this.idLivreur = idLivreur;
        this.date = date;
        this.etatLivraison = etatLivraison;
        this.livreur = livreur;
        this.article =article;
       
    }

    public Livreur getLivreur() {
        return livreur;
    }

    public void setLivreur(Livreur livreur) {
        this.livreur = livreur;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

  
    
   

    public int getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(int idCommande) {
        this.idCommande = idCommande;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public int getIdSociete() {
        return idSociete;
    }

    public void setIdSociete(int idSociete) {
        this.idSociete = idSociete;
    }

    public int getIdLivreur() {
        return idLivreur;
    }

    public void setIdLivreur(int idLivreur) {
        this.idLivreur = idLivreur;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getEtatLivraison() {
        return etatLivraison;
    }

    public void setEtatLivraison(String etatLivraison) {
        this.etatLivraison = etatLivraison;
    }

    @Override
    public String toString() {
        return "Commande{" + "idCommande=" + idCommande + ", idUtilisateur=" + idUtilisateur + ", idSociete=" + idSociete + ", idLivreur=" + idLivreur + ", date=" + date + ", etatLivraison=" + etatLivraison + ", livreur=" + livreur + ", article=" + article + '}';
    }

    

   
    
    
    
}
