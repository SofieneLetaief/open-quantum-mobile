/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entites;

/**
 *
 * @author med talel
 */
public class LigneCommade {
    
    
    public class LigneCommande {
    
    
    /*private int codeArticle;
    private int qteArticle; 
    private int prixArticle; */
    
    private int idLigneComande,idCommande,idArticle,quantite;
    
    
    
    public LigneCommande(){
        
    }

    public LigneCommande(int idLigneComande,int idComande, int idArticle, int quantite) {
        this.idLigneComande = idLigneComande;
         this.idCommande = idComande;
        this.idArticle = idArticle;
        this.quantite = quantite;
    }

        public int getIdCommande() {
            return idCommande;
        }

        public void setIdCommande(int idCommande) {
            this.idCommande = idCommande;
        }
    
    

    public int getIdLigneComande() {
        return idLigneComande;
    }

    public void setIdLigneComande(int idLigneComande) {
        this.idLigneComande = idLigneComande;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public void setIdArticle(int idArticle) {
        this.idArticle = idArticle;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }
    
     public void ajouterQuantite(int qte){
       
        this.quantite=+qte;
    }
    
    public int getPrixLigne(){
        
        int prix =this.getQuantite()*this.getPrixLigne();
        return prix;
    }

        @Override
        public String toString() {
            return "LigneCommande{" + "idLigneComande=" + idLigneComande + ", idCommande=" + idCommande + ", idArticle=" + idArticle + ", quantite=" + quantite + '}';
        }
    
    
   
    
   
}

    
}
