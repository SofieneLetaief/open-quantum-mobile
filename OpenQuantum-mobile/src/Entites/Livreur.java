/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;

/**
 *
 * @author HP OMEN
 */
public class Livreur {
    
    private String nom,prenom,adresse;
    private String image;
    private double idLivreur,numtel;
    public Livreur(){
        
    }
    
    public Livreur(double idLivreur, double numtel, String nom, String prenom, String adresse, String image) {
        this.idLivreur = idLivreur;
        this.numtel = numtel;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.image = image;
    }

    public double getIdLivreur() {
        return idLivreur;
    }

    public void setIdLivreur(double idLivreur) {
        this.idLivreur = idLivreur;
    }

    public double getNumtel() {
        return numtel;
    }

    public void setNumtel(double numtel) {
        this.numtel = numtel;
    }

    
    
    
    
 
 


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Livreur{" + "idLivreur=" + idLivreur + ", numtel=" + numtel + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" + adresse + ", image=" + image + '}';
    }
    
    
    
    
}
