/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entites;

/**
 *
 * @author omar
 */
public class FluxMsg {
    
    private int idSender;
    private int idReciever,idMsg;
    private String content;
    private boolean seen;

    public FluxMsg(int idSender, int idReciever, String content,boolean seen) {
        this.idSender = idSender;
        this.idReciever = idReciever;
        this.content = content;
        this.seen=seen;
    }

    public FluxMsg(int idMsg,int idSender, int idReciever, String content, boolean seen) {
        this.idSender = idSender;
        this.idReciever = idReciever;
        this.idMsg = idMsg;
        this.content = content;
        this.seen = seen;
    }
    
    
    

    public int getIdMsg() {
        return idMsg;
    }

    public void setIdMsg(int idMsg) {
        this.idMsg = idMsg;
    }
    
    

    public FluxMsg() {
    }

    public int getIdSender() {
        return idSender;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public void setIdSender(int idSender) {
        this.idSender = idSender;
    }

    public int getIdReciever() {
        return idReciever;
    }

    public void setIdReciever(int idReciever) {
        this.idReciever = idReciever;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "FluxMsg{" + "idSender=" + idSender + ", idReciever=" + idReciever + ", content=" + content + ", seen=" + seen + '}';
    }

    
    
    
    
    
}
